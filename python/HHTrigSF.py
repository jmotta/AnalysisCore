import os
import envyaml
from copy import deepcopy as copy

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from analysis_tools.utils import import_root, getContentHisto3D
from Base.Modules.baseModules import JetLepMetSyst
from Tools.Tools.utils_trigger import getSingleCrossThresholds

ROOT = import_root()

corrCfg = envyaml.EnvYAML('%s/src/Tools/Tools/data/TriggerScaleFactorsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class HH_trigSFRDFProducer_Run2(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_trigSFRDFProducer_Run2, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += self.runPeriod
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.year == 2016:
            mutau_pt_th1 = 23.
            mutau_pt_th2 = 25.
            etau_pt_th1 = -1.
            etau_pt_th2 = -1.
        elif self.year in [2017, 2018]:
            mutau_pt_th1 = 25.
            mutau_pt_th2 = 32.
            etau_pt_th1 = 33.
            etau_pt_th2 = 35.

        mutau_pt_th1 = 25.
        mutau_pt_th2 = 32.
        etau_pt_th1 = 31.
        etau_pt_th2 = 35.

        if self.isMC:
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            ROOT.gROOT.ProcessLine(".L $CMSSW_BASE/src/Tools/Tools/interface/HHTrigSFinterface.h")

            ROOT.gInterpreter.Declare("""
                auto HH_trigSF = HHTrigSFinterface(%s, %s, %s, %s, %s,
                    "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s");
            """ % (int(self.year), mutau_pt_th1, mutau_pt_th2, etau_pt_th1, etau_pt_th2,
                   corrCfg[self.corrKey]["eTrgSF"], corrCfg[self.corrKey]["eTauTrgSF"],
                   corrCfg[self.corrKey]["muTrgSF"], corrCfg[self.corrKey]["muTauTrgSF"],
                   corrCfg[self.corrKey]["tauTrgSF_ditau"], corrCfg[self.corrKey]["tauTrgSF_mutau"],
                   corrCfg[self.corrKey]["tauTrgSF_etau"], corrCfg[self.corrKey]["tauTrgSF_vbf"], 
                   corrCfg[self.corrKey]["jetTrgSF_vbf"]))

    def run(self, df):
        if not self.isMC:
            return df, []
        
        branches = ['trigSF', 'trigSF_single', 'trigSF_cross',
                    'trigSF_muUp', 'trigSF_muDown', 'trigSF_eleUp', 'trigSF_eleDown',
                    'trigSF_DM0Up', 'trigSF_DM1Up', 'trigSF_DM10Up', 'trigSF_DM11Up', 
                    'trigSF_DM0Down', 'trigSF_DM1Down', 'trigSF_DM10Down', 'trigSF_DM11Down',
                    'trigSF_vbfjetUp', 'trigSF_vbfjetDown']
        
        df = df.Define("HH_trigsf", "HH_trigSF.get_hh_trigsf("
                       "pairType, isVBFtrigger, "
                       "dau1_index, dau2_index, VBFjet1_JetIdx, VBFjet2_JetIdx, "
                       f"Muon_pt{self.muon_syst}, Muon_eta, Electron_pt{self.electron_syst}, "
                       f"Electron_eta, Tau_pt{self.tau_syst}, Tau_eta, Tau_decayMode, "
                       f"Jet_pt{self.jet_syst}, Jet_eta, Jet_phi, Jet_mass{self.jet_syst})")
        
        for ib, branch in enumerate(branches):
            df = df.Define(branch, "HH_trigsf[%s]" % (ib))
        return df, branches


class HH_trigSFRDFProducer_Run3(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_trigSFRDFProducer_Run3, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")
        self.tauvsjet_wp = kwargs.pop("tauvsjet_wp")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)
        
        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass

        prefix += self.runPeriod

        self.corrKey = prefix + year

        (isomuTrg_offMuThr, mutauTrg_offMuThr, mutauTrg_offTauThr, 
         eleTrg_offEleThr, etauTrg_offEleThr, etauTrg_offTauThr,
         ditauTrg_offTauThr, ditaujetTrg_offTauThr,
         ditaujetTrg_offJetThr) = getSingleCrossThresholds(self.year, self.runPeriod)

        if self.isMC:
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            ROOT.gROOT.ProcessLine(".L $CMSSW_BASE/src/Tools/Tools/interface/HHTrigSFinterface_corrlib.h")
            
            ROOT.gInterpreter.Declare("""
                auto HH_trigSF = HHTrigSFinterface_corrlib(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "%s", "%s",
                    "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s");
            """ % (int(self.year), isomuTrg_offMuThr, mutauTrg_offMuThr,
                   mutauTrg_offTauThr, eleTrg_offEleThr, etauTrg_offEleThr, 
                   etauTrg_offTauThr, ditauTrg_offTauThr,
                   ditaujetTrg_offTauThr, ditaujetTrg_offJetThr,
                   corrCfg[self.corrKey]["eTrgSF_file"], corrCfg[self.corrKey]["eCorrHltName"], corrCfg[self.corrKey]["eCorrYearTag"],
                   corrCfg[self.corrKey]["muTrgSF_file"], corrCfg[self.corrKey]["muCorrHltName"],
                   corrCfg[self.corrKey]["tauTrgSF_file"],
                   corrCfg[self.corrKey]["eTauTrgSF_file"], corrCfg[self.corrKey]["eTauCorrHltName"],
                   corrCfg[self.corrKey]["muTauTrgSF_file"], corrCfg[self.corrKey]["muTauCorrHltName"],
                   corrCfg[self.corrKey]["taujetTrgSF_file"], corrCfg[self.corrKey]["tauJetCorrHltName"],
                   corrCfg[self.corrKey]["vbfTrgSF_file"], "None", "None"))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = ['trigSF',
                    'trigSF_muUp', 'trigSF_muDown', 'trigSF_eleUp', 'trigSF_eleDown',
                    'trigSF_DM0Up', 'trigSF_DM1Up', 'trigSF_DM10Up', 'trigSF_DM11Up', 
                    'trigSF_DM0Down', 'trigSF_DM1Down', 'trigSF_DM10Down', 'trigSF_DM11Down',
                    'trigSF_vbfjetUp', 'trigSF_vbfjetDown', 'trigSF_jetUp', 'trigSF_jetDown',
                    'trigSF_nojetSF'] # JM FIXME: temporary additional trigSF without jet leg SFs

        # store systematics impact on SF only for the central production;
        # skip them for all systematic variated selections
        if self.skip_unused_systs and self.systs != "":
            branches = ['trigSF']

        df = df.Define("HH_trigsf", "HH_trigSF.get_hh_trigsf("
                       "pairType, isVBFtrigger, bjet1_JetIdx, fatjet_JetIdx, "
                       "dau1_index, dau2_index, VBFjet1_JetIdx, VBFjet2_JetIdx, "
                       f"Muon_pt{self.muon_syst}, Muon_eta, Electron_pt{self.electron_syst}, "
                       f'Electron_eta, Tau_pt{self.tau_syst}, Tau_eta, Tau_decayMode, "{self.tauvsjet_wp}", '
                       f"Jet_pt{self.jet_syst}, Jet_eta, Jet_phi, Jet_mass{self.jet_syst}, "
                       f"FatJet_pt{self.jet_syst}, FatJet_eta)")

        for branch in branches:
            df = df.Define(branch, f"HH_trigsf.{branch}")
        return df, branches


def HH_trigSFRDF(**kwargs):
    year = kwargs.get("year")

    if year <= 2018:
        return lambda: HH_trigSFRDFProducer_Run2(**kwargs)

    elif year >= 2022:
        return lambda: HH_trigSFRDFProducer_Run3(**kwargs)

    else:
        raise ValueError(f"Requested year {year} not available; "
                          "trigSF modules implemented only for Run-2 or Run-3")

