import re
import os
from math import sqrt
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

def Phi_mpi_pi(x):
    while (x >= 3.14159):
        x -= (2 * 3.14159)
    while (x < -3.14159):
        x += (2 * 3.14159)
    return x;

def deltaR(obj1_eta, obj1_phi, obj2_eta, obj2_phi):
    deta = obj1_eta - obj2_eta
    dphi = Phi_mpi_pi(obj1_phi - obj2_phi)
    return sqrt(deta * deta + dphi * dphi)


# extracted from https://github.com/cms-tau-pog/TauFW/blob/master/PicoProducer/python/analysis/utils.py
class LeptonTauPair:
    """Container class to pair and order tau decay candidates."""
    def __init__(self, obj1, pt1, iso1, obj2, pt2, iso2):
        self.obj1 = obj1
        self.obj2 = obj2
        self.pt1  = pt1
        self.pt2  = pt2
        self.iso1 = iso1
        self.iso2 = iso2
        self.pair = [obj1, obj2]
      
    def __gt__(self, opair):
        if self.iso1 != opair.iso1:
            return self.iso1 > opair.iso1 # greater = smaller lepton isolation
        elif self.pt1 != opair.pt1:
            return self.pt1 > opair.pt1  # greater = higher pT
        elif self.iso2 != opair.iso2:
            return self.iso2 > opair.iso2 # greater = smaller tau isolation
        elif self.pt2 != opair.pt2:
            return self.pt2 > opair.pt2  # greater = higher pT
        return True

    def check_charge(self):
        if (self.obj1.charge + self.obj2.charge == 0) and abs(self.obj1.charge) == 1:
            return True
        else:
            return False


def lepton_veto(electrons, muons, taus, obj=None):
    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Common_lepton_vetoes
    nleps = 0
    # check extra muon veto

    # print "muons: "
    for muon in muons:
        # print muon.pt, muon.eta, muon.phi, muon.dz, muon.dxy, muon.pfRelIso04_all, muon.mediumId, muon.tightId,
        if obj == muon:
            # print "mymuon"
            continue
        # if any([muon.DeltaR(tau) < 0.4 for tau in taus]):
            # print "deltaR"
            # continue
        if (abs(muon.eta) > 2.4 or muon.pt < 10 or abs(muon.dz) > 0.2
                or abs(muon.dxy) > 0.045 or muon.pfRelIso04_all > 0.3):
            # print "stuff"
            continue
        if not (muon.mediumId or muon.tightId):
            continue
        nleps += 1

    # check extra electron veto
    # print "electrons: "
    for electron in electrons:
        # print electron.pt, electron.eta, electron.phi, electron.dz, electron.dxy, electron.pfRelIso03_all, electron.convVeto, electron.lostHits, electron.mvaFall17V2Iso_WP90,
        if obj == electron:
            # print "mye"
            continue
        # if any([electron.DeltaR(tau) < 0.4 for tau in taus]):
            # print "deltaR"
            # continue
        if (abs(electron.eta) > 2.5 or electron.pt < 10 or abs(electron.dz) > 0.2
                or abs(electron.dxy) > 0.045):
            continue
        if not ((electron.pfRelIso03_all < 0.3 and electron.mvaFall17V2noIso_WP90)
                or electron.mvaFall17V2Iso_WP90):
            continue
        # if electron.convVeto == 1 and electron.lostHits <= 1:
            # print
        nleps += 1
        # print "cosas raras"

    return (nleps > 0), nleps
