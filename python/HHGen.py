import os
from analysis_tools.utils import import_root

ROOT = import_root()

class HHGenRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.isSignal = False

        dataset = kwargs.pop("dataset")
        if "GluGlutoHHto2B2Tau" in dataset or "VBFHHto2B2Tau" in dataset:
            self.isSignal = True

        if self.isMC and self.isSignal:
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/HHGenInterface.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto HHGen = HHGenInterface();
            """)

    def run(self, df):
        if not (self.isMC and self.isSignal):
            return df, []

        variables = ["H1_idx", "H1_pt", "H1_eta", "H1_phi", "H1_mass",
                     "H2_idx", "H2_pt", "H2_eta", "H2_phi", "H2_mass",
                     "L1_idx", "L1_pt", "L1_eta", "L1_phi", "L1_mass", "L1_pdgid",
                     "L2_idx", "L2_pt", "L2_eta", "L2_phi", "L2_mass", "L2_pdgid",
                     "B1_idx", "B1_pt", "B1_eta", "B1_phi", "B1_mass", "B1_pdgid",
                     "B2_idx", "B2_pt", "B2_eta", "B2_phi", "B2_mass", "B2_pdgid",
                     "HH_pt", "HH_eta", "HH_phi", "HH_mass", "HH_cts",
                     "LL_pt", "LL_eta", "LL_phi", "LL_mass", "PairType",
                     "BB_pt", "BB_eta", "BB_phi", "BB_mass"]

        df = df.Define("hh_gen_results", "HHGen.get_gen_info("
                            "GenPart_statusFlags, GenPart_pdgId, GenPart_status, "
                            "GenPart_genPartIdxMother, GenPart_pt, GenPart_eta, "
                            "GenPart_phi, GenPart_mass)")

        branches = []
        for var in variables:
            df = df.Define(f"gen{var}", f"hh_gen_results.{var}")
            branches.append(f"gen{var}")

        return df, branches

def HHGenRDF(**kwargs):
    """
    Module to obtain HH gernerator level information

    :param isMC: flag of the dataset being MC or data
    :type : bool

    :param isMC: name of the dataset
    :type : str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHGenRecoRDF
            path: Tools.Tools.HHGen
            parameters:
                isMC: self.dataset.process.isMC
                dataset: self.dataset.name
    """
    return lambda: HHGenRDFProducer(**kwargs)


class HHGen2RecoRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.isSignal = False

        dataset = kwargs.pop("dataset")
        if "GluGlutoHHto2B2Tau" in dataset or "VBFHHto2B2Tau" in dataset:
            self.isSignal = True

        if self.isMC and self.isSignal:
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/HHGenInterface.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto HHGen = HHGenInterface();
            """)

    def run(self, df):
        if not (self.isMC and self.isSignal):
            return df, []

        gen_variables = ["H1_idx", "H1_pt", "H1_eta", "H1_phi", "H1_mass",
                         "H2_idx", "H2_pt", "H2_eta", "H2_phi", "H2_mass",
                         "L1_idx", "L1_pt", "L1_eta", "L1_phi", "L1_mass", "L1_pdgid",
                         "L2_idx", "L2_pt", "L2_eta", "L2_phi", "L2_mass", "L2_pdgid",
                         "B1_idx", "B1_pt", "B1_eta", "B1_phi", "B1_mass", "B1_pdgid",
                         "B2_idx", "B2_pt", "B2_eta", "B2_phi", "B2_mass", "B2_pdgid",
                         "HH_pt", "HH_eta", "HH_phi", "HH_mass", "HH_cts",
                         "LL_pt", "LL_eta", "LL_phi", "LL_mass", "PairType",
                         "BB_pt", "BB_eta", "BB_phi", "BB_mass"]
        reco_variables = ["T1_idx", "T1_pt", "T1_eta", "T1_phi", "T1_mass",
                          "T2_idx", "T2_pt", "T2_eta", "T2_phi", "T2_mass",    
                          "J1_idx", "J1_pt", "J1_eta", "J1_phi", "J1_mass",
                          "J2_idx", "J2_pt", "J2_eta", "J2_phi", "J2_mass",
                          "TT_pt", "TT_eta", "TT_phi", "TT_mass",
                          "JJ_pt", "JJ_eta", "JJ_phi", "JJ_mass"]

        df = df.Define("hh_gen_reco_results", "HHGen.get_gen_reco("
                            "GenPart_statusFlags, GenPart_pdgId, "
                            "GenPart_status, GenPart_genPartIdxMother, "
                            "GenPart_pt, GenPart_eta, GenPart_phi, GenPart_mass, "
                            "GenVisTau_genPartIdxMother, GenVisTau_status, "
                            "GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,"
                            "Muon_genPartIdx, Muon_pt, Muon_eta, Muon_phi, Muon_mass, "
                            "Electron_genPartIdx, Electron_pt, Electron_eta, Electron_phi, Electron_mass, "
                            "Tau_genPartIdx, Tau_pt, Tau_eta, Tau_phi, Tau_mass, "
                            "GenJet_partonFlavour, GenJet_pt, GenJet_eta, GenJet_phi, GenJet_mass)")

        branches = []
        for var in gen_variables:
            df = df.Define(f"gen{var}", f"hh_gen_reco_results.{var}")
            branches.append(f"gen{var}")

        for var in reco_variables:
            df = df.Define(f"reco{var}", f"hh_gen_reco_results.{var}")
            branches.append(f"reco{var}")

        return df, branches

def HHGen2RecoRDF(**kwargs):
    """
    Module to obtain HH gernerator level information associated to the reconstructed
    objects matched to the generator level ones

    :param isMC: flag of the dataset being MC or data
    :type : bool

    :param isMC: name of the dataset
    :type : str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHGen2RecoRDF
            path: Tools.Tools.HHGen
            parameters:
                isMC: self.dataset.process.isMC
                dataset: self.dataset.name
    """
    return lambda: HHGen2RecoRDFProducer(**kwargs)


class HHMaxGenMassRDFProducer():
    def __init__(self):
        ROOT.gInterpreter.Declare("""
            #include <TLorentzVector.h>
            using Vfloat = const ROOT::RVec<float>&;
            double get_max_invariant_mass(Vfloat pt, Vfloat eta, Vfloat phi, Vfloat mass) {
                double max_inv_mass = -999.;
                for (size_t i = 0; i < pt.size() - 1; i++) {
                    for (size_t j = i + 1; j < pt.size(); j++) {
                        auto jet1 = TLorentzVector();
                        auto jet2 = TLorentzVector();
                        auto jj = TLorentzVector();
                        jet1.SetPtEtaPhiM(pt[i], eta[i], phi[i], mass[i]);
                        jet2.SetPtEtaPhiM(pt[j], eta[j], phi[j], mass[j]);
                        jj = jet1 + jet2;
                        if (jj.M() > max_inv_mass)
                            max_inv_mass = jj.M();
                    }
                }
                return max_inv_mass;
            }
        """)

    def run(self, df):
        df = df.Define("max_genjj_mass",
            "get_max_invariant_mass(GenJet_pt, GenJet_eta, GenJet_phi, GenJet_mass)")
        return df, ["max_genjj_mass"]

def HHMaxGenMassRDF():
    return lambda: HHMaxGenMassRDFProducer()


class HHMaxGenDeltaEtaRDFProducer():
    def __init__(self):
        ROOT.gInterpreter.Declare("""
            #include <TLorentzVector.h>
            using Vfloat = const ROOT::RVec<float>&;
            double get_max_delta_eta(Vfloat eta) {
                double max_delta_eta = -999.;
                for (size_t i = 0; i < eta.size() - 1; i++) {
                    for (size_t j = i + 1; j < eta.size(); j++) {
                        if (fabs(eta[i] - eta[j]) > max_delta_eta)
                            max_delta_eta = fabs(eta[i] - eta[j]);
                    }
                }
                return max_delta_eta;
            }
        """)

    def run(self, df):
        df = df.Define("max_genjj_delta_eta", "get_max_delta_eta(GenJet_eta)")
        return df, ["max_genjj_delta_eta"]

def HHMaxGenDeltaEtaRDF():
    return lambda: HHMaxGenDeltaEtaRDFProducer()


class LHEVPtFilterRDFProducer():
    def __init__(self, *args, **kwargs):
        self.filterVPt = kwargs.pop("filterVPt", False)
        self.maxVPt = kwargs.pop("maxVPt", 100000) # default = 100 TeV
        self.minVPt = kwargs.pop("minVPt", 0)      # default =   0 GeV (no cut)

        self.VPt_selection = "(LHE_Vpt >= {min} && LHE_Vpt < {max})".format(
            min = str(self.minVPt),
            max = str(self.maxVPt)
        )

    def run(self, df):
        if not self.filterVPt:
            return df, []

        df = df.Filter(self.VPt_selection)
        return df, []

def LHEVPtFilterRDF(**kwargs):
    """
    Module to filter events based on the LHE_Vpt value.

    :param filterVPt: apply the filter or not
    :type : bool

    :param maxVPt: maximum value of LHE_Vpt allowed
    :type : int/float

    :param minVPt: minimum value of LHE_Vpt allowed
    :type : int/float

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: LHEVPtFilterRDF
            path: Tools.Tools.HHGen
            parameters:
                filterVPt: self.dataset.process.get_aux('filterVPt', False)
                minVPt: self.dataset.process.get_aux('minVPt', 0)
                maxVPt: self.dataset.process.get_aux('maxVPt', 100000)
    """
    return lambda: LHEVPtFilterRDFProducer(**kwargs)
