import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root

ROOT = import_root()

class HHVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHVarRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.noJER = kwargs.pop("noJER", False)
        self.isZZAnalysis = kwargs.pop("isZZAnalysis", False)
        self.bypass_HHKinFit = kwargs.pop("bypass_HHKinFit", False)
        self.ctgrName = kwargs.pop("ctgrName")
        self.flatNTuple = kwargs.pop("flatNTuple", False)

        localIsMC = ("true" if self.isMC else "false")
        self.jet_resolution = "jet_pt_resolution" if self.isMC else "Jet_eta"
        if self.noJER:
            localIsMC = "false"
            self.jet_resolution = "Jet_eta" # dummy/placholder

        if not self.isZZAnalysis:
            KinFit_targetM1 = 125
            KinFit_targetM2 = 125
        else:
            KinFit_targetM1 = 91
            KinFit_targetM2 = 91

        if not os.getenv("_HHVAR_%s" % self.systs):
            os.environ["_HHVAR_%s" % self.systs] = "hhvar"

            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/HHVariablesInterface.h".format(base))

            ROOT.gInterpreter.Declare("""
                auto HHcomputer = HHVariablesInterface(%s, %i, %i);
            """ % (localIsMC, KinFit_targetM1, KinFit_targetM2))

    def run(self, df):
        variables_str = (f"Htt_pt{self.lep_syst},Htt_eta{self.lep_syst},Htt_phi{self.lep_syst},Htt_mass{self.lep_syst},"
                         f"Htt_met_pt{self.systs},Htt_met_eta{self.systs},Htt_met_phi{self.systs},Htt_met_mass{self.systs},"
                         f"Hbb_pt{self.jet_syst},Hbb_eta{self.jet_syst},Hbb_phi{self.jet_syst},Hbb_mass{self.jet_syst},"
                         f"HH_pt{self.systs},HH_eta{self.systs},HH_phi{self.systs},HH_mass{self.systs},"
                         f"HH_svfit_pt{self.systs},HH_svfit_eta{self.systs},HH_svfit_phi{self.systs},HH_svfit_mass{self.systs},"
                         f"HH_kinfit_mass{self.systs},HH_kinfit_chi2{self.systs},"
                         f"VBFjj_deltaEta{self.systs},VBFjj_deltaPhi{self.systs},VBFjj_mass{self.systs}")
        variables = variables_str.split(",")

        if self.flatNTuple:
            df = df.Define(f"hhfeatures{self.systs}",
                    f"HHcomputer.computeHHfeatures(\"{self.ctgrName}\", "
                    "pairType, hasBoostedAK8, hasResolvedAK4, hasVBFAK4, "
                    f"dau1_pt{self.lep_syst}, dau1_eta, dau1_phi, dau1_mass{self.lep_syst}, "
                    f"dau2_pt{self.lep_syst}, dau2_eta, dau2_phi, dau2_mass{self.lep_syst}, "
                    f"bjet1_pt{self.jet_syst}, bjet1_eta, bjet1_phi, bjet1_mass{self.jet_syst}, bjet1_resolution, "
                    f"bjet2_pt{self.jet_syst}, bjet2_eta, bjet2_phi, bjet2_mass{self.jet_syst}, bjet2_resolution, "
                    f"fatbjet_pt{self.jet_syst}, fatbjet_eta, fatbjet_phi, fatbjet_mass{self.jet_syst}, "
                    f"vbfjet1_pt{self.jet_syst}, vbfjet1_eta, vbfjet1_phi, vbfjet1_mass{self.jet_syst}, "
                    f"vbfjet2_pt{self.jet_syst}, vbfjet2_eta, vbfjet2_phi, vbfjet2_mass{self.jet_syst}, "
                    f"PuppiMET{self.met_smear_tag}_pt{self.met_syst}, PuppiMET{self.met_smear_tag}_phi{self.met_syst}, "
                    f"MET_covXX, MET_covXY, MET_covYY, Htt_svfit_pt{self.systs},"
                    f"Htt_svfit_eta{self.systs}, Htt_svfit_phi{self.systs}, Htt_svfit_mass{self.systs}, "
                    "{0})".format("true" if self.bypass_HHKinFit else "false"))

        else:
            df = df.Define(f"hhfeatures{self.systs}",
                    f"HHcomputer.computeHHfeatures(\"{self.ctgrName}\", "
                    "hasBoostedAK8, hasResolvedAK4, hasVBFAK4, "
                    "pairType, dau1_index, dau2_index, "
                    "bjet1_JetIdx, bjet2_JetIdx, fatjet_JetIdx, "
                    "VBFjet1_JetIdx, VBFjet2_JetIdx, "
                    f"Muon_pt{self.muon_syst}, Muon_eta, Muon_phi, Muon_mass{self.muon_syst}, "
                    f"Electron_pt{self.electron_syst}, Electron_eta, Electron_phi, Electron_mass{self.electron_syst}, "
                    f"Tau_pt{self.tau_syst}, Tau_eta, Tau_phi, Tau_mass{self.tau_syst}, "
                    f"Jet_pt{self.jet_syst}, Jet_eta, Jet_phi, Jet_mass{self.jet_syst}, {self.jet_resolution}, "
                    f"FatJet_pt{self.jet_syst}, FatJet_eta, FatJet_phi, FatJet_mass{self.jet_syst}, "
                    f"PuppiMET{self.met_smear_tag}_pt{self.met_syst}, PuppiMET{self.met_smear_tag}_phi{self.met_syst}, "
                    f"MET_covXX, MET_covXY, MET_covYY, Htt_svfit_pt{self.systs},"
                    f"Htt_svfit_eta{self.systs}, Htt_svfit_phi{self.systs}, Htt_svfit_mass{self.systs}, "
                    "{0})".format("true" if self.bypass_HHKinFit else "false"))

        features = []
        for var in variables:
            varStructName = var.replace(self.lep_syst,'').replace(self.systs,'').replace(self.jet_syst,'')
            df = df.Define(f"{var}", f"hhfeatures{self.systs}.{varStructName}")
            features.append(f"{var}")

        return df, features

def HHVarRDF(*args, **kwargs):
    return lambda: HHVarRDFProducer(*args, **kwargs)
