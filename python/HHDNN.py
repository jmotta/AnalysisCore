import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from analysis_tools.utils import import_root
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()


class HHDNNInputRDFProducer(JetLepMetSyst):
    def __init__(self, isZZAnalysis, *args, **kwargs):
        super(HHDNNInputRDFProducer, self).__init__(*args, **kwargs)
        self.year = kwargs.pop("year")
        self.isZZAnalysis = isZZAnalysis

        if not os.getenv("_HHbbttDNNDefault"):
            os.environ["_HHbbttDNNDefault"] = "HHbbttDNNDefault"

            if os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc10":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/eigen/d812f411c3f9-cms/include/")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/tensorflow/2.5.0/include/")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc820":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/eigen/d812f411c3f9-bcolbf/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/tensorflow/2.1.0-bcolbf/include")

            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "el9_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/tensorflow/2.12.0-56ca17f4dd95f093a501c2ddea2603f2/include")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/tensorflow/2.12.0-a78fc3db7a6d73e4417418af1388a3c7/include")

            else:
                raise ValueError("Architecture not considered")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gSystem.Load("libToolsTools.so")
            ROOT.gROOT.ProcessLine(".L {}/interface/HHDNNinterface.h".format(base))
            ROOT.gROOT.ProcessLine(".L {}/interface/lester_mt2_bisect.h".format(base))

        if not os.getenv("_HHbbttDNNInput"):
            os.environ["_HHbbttDNNInput"] = "HHbbttDNNInput"

            feat_file = "{}/{}/src/cms_runII_dnn_models/models/arc_checks/zz_bbtt/2021-11-22-0/" \
                "features.txt".format(os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            with open(feat_file) as f:
                self.default_feat = [i.split('\n')[0] for i in f.readlines()]

            if self.isZZAnalysis:
                model_dir = "{}/{}/src/cms_runII_dnn_models/models/arc_checks/zz_bbtt/2023-08-02-0/".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            else:
                model_dir = "{}/{}/src/cms_runII_dnn_models/models/nonres_gluglu/2020-07-31-0/".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ensemble = model_dir + "ensemble"
            features = model_dir + "features.txt"

            feature_file = kwargs.pop("feature_file", features)
            with open(feature_file) as f:
                lines = f.readlines()
            req_features = ', '.join(['"%s"' % line.strip() for line in lines])

            model_dir = kwargs.pop("model_dir", ensemble)

            ROOT.gInterpreter.Declare("""
                auto hhdnnInput = HHDNNinterface("%s", {%s}, {1.}, %s);
            """ % (model_dir, req_features, self.year))

            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<Float_t>&;
                ROOT::RVec<Float_t> get_dnn_inputs(int pairType, int hasBoostedAK8, int event,
                    int dau1_index, int dau2_index, int bjet1_index, int bjet2_index,
                    int vbfjet1_index, int vbfjet2_index,
                    Vfloat muon_pt, Vfloat muon_eta, Vfloat muon_phi, Vfloat muon_mass,
                    Vfloat electron_pt, Vfloat electron_eta, Vfloat electron_phi, Vfloat electron_mass,
                    Vfloat tau_pt, Vfloat tau_eta, Vfloat tau_phi, Vfloat tau_mass,
                    Vfloat jet_pt, Vfloat jet_eta, Vfloat jet_phi, Vfloat jet_mass,
                    float htt_sv_pt, float htt_sv_eta, float htt_sv_phi, float htt_sv_mass,
                    float HHKinFit_mass, float HHKinFit_chi2, float met_pt, float met_phi,
                    Vfloat Jet_btagDeepFlavB, Vfloat Jet_btagDeepFlavCvL, Vfloat Jet_btagDeepFlavCvB,
                    Vfloat Jet_HHbtag
                )
                {
                    float dau1_pt, dau1_eta, dau1_phi, dau1_mass, dau2_pt, dau2_eta, dau2_phi, dau2_mass;

                    // pairType | Our Def. | DNN
                    // ------------------------
                    // mutau   |    0     |  1
                    // etau    |    1     |  2
                    // tautau  |    2     |  0

                    int channel;
                    if(pairType == 0)
                        channel = 1;
                    else if (pairType == 1)
                        channel = 2;
                    else
                        channel = 0;

                    if (pairType == 0) {
                        dau1_pt = muon_pt.at(dau1_index);
                        dau1_eta = muon_eta.at(dau1_index);
                        dau1_phi = muon_phi.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                    } else if (pairType == 1) {
                        dau1_pt = electron_pt.at(dau1_index);
                        dau1_eta = electron_eta.at(dau1_index);
                        dau1_phi = electron_phi.at(dau1_index);
                        dau1_mass = electron_mass.at(dau1_index);
                    } else if (pairType == 2) {
                        dau1_pt = tau_pt.at(dau1_index);
                        dau1_eta = tau_eta.at(dau1_index);
                        dau1_phi = tau_phi.at(dau1_index);
                        dau1_mass = tau_mass.at(dau1_index);
                    }
                    dau2_pt = tau_pt.at(dau2_index);
                    dau2_eta = tau_eta.at(dau2_index);
                    dau2_phi = tau_phi.at(dau2_index);
                    dau2_mass = tau_mass.at(dau2_index);

                    auto dau1_tlv = TLorentzVector();
                    auto dau2_tlv = TLorentzVector();
                    auto bjet1_tlv = TLorentzVector();
                    auto bjet2_tlv = TLorentzVector();
                    auto vbfjet1_tlv = TLorentzVector();
                    auto vbfjet2_tlv = TLorentzVector();

                    dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
                    dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
                    bjet1_tlv.SetPtEtaPhiM(jet_pt.at(bjet1_index), jet_eta.at(bjet1_index),
                        jet_phi.at(bjet1_index), jet_mass.at(bjet1_index));
                    bjet2_tlv.SetPtEtaPhiM(jet_pt.at(bjet2_index), jet_eta.at(bjet2_index),
                        jet_phi.at(bjet2_index), jet_mass.at(bjet2_index));

                    int nvbf = 0;
                    if (vbfjet1_index >= 0) {
                        vbfjet1_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet1_index), jet_eta.at(vbfjet1_index),
                            jet_phi.at(vbfjet1_index), jet_mass.at(vbfjet1_index));
                        vbfjet2_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet2_index), jet_eta.at(vbfjet2_index),
                            jet_phi.at(vbfjet2_index), jet_mass.at(vbfjet2_index));
                        nvbf = 2;
                    } else {
                        vbfjet1_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
                        vbfjet2_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
                    }
                    auto met_tlv = TLorentzVector();
                    met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);

                    auto htt_svfit_tlv = TLorentzVector();
                    if (htt_sv_mass > 0)
                        htt_svfit_tlv.SetPtEtaPhiM(htt_sv_pt, htt_sv_eta, htt_sv_phi, htt_sv_mass);
                    else
                        htt_svfit_tlv.SetPtEtaPhiM(1., 1., 1., 1.);

                    // MT2 computation
                    asymm_mt2_lester_bisect::disableCopyrightMessage();
                    double MT2 = asymm_mt2_lester_bisect::get_mT2(
                        bjet1_tlv.M(), bjet1_tlv.Px(), bjet1_tlv.Py(),
                        bjet2_tlv.M(), bjet2_tlv.Px(), bjet2_tlv.Py(),
                        dau1_tlv.Px() + dau2_tlv.Px() + met_tlv.Px(),
                        dau1_tlv.Py() + dau2_tlv.Py() + met_tlv.Py(),
                        dau1_tlv.M(), dau2_tlv.M(), 0.);

                    float deepFlav1 = -1., deepFlav2 = -1., CvsL_b1 = -1., CvsL_b2 = -1.,
                        CvsL_vbf1 = -1., CvsL_vbf2 = -1., CvsB_b1 = -1., CvsB_b2 = -1.,
                        CvsB_vbf1 = -1., CvsB_vbf2 = -1., HHbtag_b1 = -1., HHbtag_b2 = -1.,
                        HHbtag_vbf1 = -1., HHbtag_vbf2 = -1.;
                    deepFlav1 = Jet_btagDeepFlavB.at(bjet1_index);
                    deepFlav2 = Jet_btagDeepFlavB.at(bjet2_index);
                    CvsL_b1 = Jet_btagDeepFlavCvL.at(bjet1_index);
                    CvsL_b2 = Jet_btagDeepFlavCvL.at(bjet2_index);
                    CvsB_b1 = Jet_btagDeepFlavCvB.at(bjet1_index);
                    CvsB_b2 = Jet_btagDeepFlavCvB.at(bjet2_index);
                    HHbtag_b1 = Jet_HHbtag.at(bjet1_index);
                    HHbtag_b2 = Jet_HHbtag.at(bjet2_index);
                    if (vbfjet1_index >= 0) {
                        CvsL_vbf1 = Jet_btagDeepFlavCvL.at(vbfjet1_index);
                        CvsL_vbf2 = Jet_btagDeepFlavCvL.at(vbfjet2_index);
                        CvsB_vbf1 = Jet_btagDeepFlavCvB.at(vbfjet1_index);
                        CvsB_vbf2 = Jet_btagDeepFlavCvB.at(vbfjet2_index);
                        HHbtag_vbf1 = Jet_HHbtag.at(vbfjet1_index);
                        HHbtag_vbf2 = Jet_HHbtag.at(vbfjet2_index);
                    }

                    return hhdnnInput.GetDeafultInputs(
                      channel, hasBoostedAK8, nvbf, event,
                      bjet1_tlv, bjet2_tlv, dau1_tlv, dau2_tlv,
                      vbfjet1_tlv, vbfjet2_tlv, met_tlv, htt_svfit_tlv,
                      HHKinFit_mass, HHKinFit_chi2, (HHKinFit_chi2 >= 0), (htt_sv_mass >= 0), MT2,
                      deepFlav1, deepFlav2, CvsL_b1, CvsL_b2, CvsL_vbf1, CvsL_vbf2,
                      CvsB_b1, CvsB_b2, CvsB_vbf1, CvsB_vbf2,
                      HHbtag_b1, HHbtag_b2, HHbtag_vbf1, HHbtag_vbf2);
                }
            """)

    def run(self, df):
        p = "H" if not self.isZZAnalysis else "Z"
        branches = ["{0}{1}".format(i, self.systs) for i in self.default_feat]
        all_branches = df.GetColumnNames()
        if branches[0] in all_branches:
            return df, []

        df = df.Define("dnn_input%s" % self.systs, "get_dnn_inputs("
            "pairType, hasBoostedAK8, event, "
            "dau1_index, dau2_index, bjet1_JetIdx, bjet2_JetIdx,VBFjet1_JetIdx, VBFjet2_JetIdx, "
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0},"
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
            "Jet_pt{3}, Jet_eta, Jet_phi, Jet_mass{3}, "
            "{7}tt_svfit_pt{4}, {7}tt_svfit_eta{4}, {7}tt_svfit_phi{4}, {7}tt_svfit_mass{4}, "
            "{7}{7}KinFit_mass{4}, {7}{7}KinFit_chi2{4}, MET{5}_pt{6}, MET{5}_phi{6}, "
            "Jet_btagDeepFlavB, Jet_btagDeepFlavCvL, Jet_btagDeepFlavCvB, Jet_HHbtag)".format(
                self.muon_syst, self.electron_syst, self.tau_syst, self.jet_syst, self.systs,
                self.met_smear_tag, self.met_syst, p)
            )

        for ib, branch in enumerate(branches):
            df = df.Define(branch, "dnn_input%s[%s]" % (self.systs, ib))
        return df, branches


class HHDNNRDFProducer(JetLepMetSyst):
    def __init__(self, isZZAnalysis, *args, **kwargs):
        super(HHDNNRDFProducer, self).__init__(*args, **kwargs)
        self.year = kwargs.pop("year")
        self.isZZAnalysis = isZZAnalysis
        self.flatNTuple = kwargs.pop("flatNTuple", False)

        if not os.getenv("_HHbbttDNNDefault"):
            os.environ["_HHbbttDNNDefault"] = "HHbbttDNNDefault"

            if os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc10":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/eigen/d812f411c3f9-cms/include/")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/tensorflow/2.5.0/include/")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc820":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/eigen/d812f411c3f9-bcolbf/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/tensorflow/2.1.0-bcolbf/include")

            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "el9_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/tensorflow/2.12.0-56ca17f4dd95f093a501c2ddea2603f2/include")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/tensorflow/2.12.0-a78fc3db7a6d73e4417418af1388a3c7/include")
            else:
                raise ValueError("Architecture not considered")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gSystem.Load("libToolsTools.so")
            ROOT.gROOT.ProcessLine(".L {}/interface/HHDNNinterface.h".format(base))

        if not os.getenv("_HHbbttDNN"):
            os.environ["_HHbbttDNN"] = "HHbbttDNN"

            if self.isZZAnalysis:
                model_dir = "{}/{}/src/cms_runII_dnn_models/models/arc_checks/zz_bbtt/2023-08-02-0/".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            else:
                model_dir = "{}/{}/src/cms_runII_dnn_models/models/nonres_gluglu/2020-07-31-0/".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ensemble = model_dir + "ensemble"
            features = model_dir + "features.txt"

            feature_file = kwargs.pop("feature_file", features)
            with open(feature_file) as f:
                lines = f.readlines()
            req_features = ', '.join(['"%s"' % line.strip() for line in lines])

            model_dir = kwargs.pop("model_dir", ensemble)

            ROOT.gInterpreter.Declare("""
                auto HHDNN = HHDNNinterface("%s", {%s}, {1.}, %s);
            """ % (model_dir, req_features, self.year))

    def run(self, df):
        p = "H" if not self.isZZAnalysis else "Z"
        branches = ["dnn_%sbbtt_kl_1%s" % (p+p, self.systs)]
        all_branches = df.GetColumnNames()
        if branches[0] in all_branches:
            return df, []

        if self.flatNTuple:
            df = df.Define("dnn_output%s" % self.systs,
                    "HHDNN.get_dnn_outputs(pairType, event, "
                    "hasResolvedAK4, hasBoostedAK8, hasVBFAK4, "
                    f"dau1_pt{self.lep_syst}, dau1_eta, dau1_phi, dau1_mass{self.lep_syst}, "
                    f"dau2_pt{self.lep_syst}, dau2_eta, dau2_phi, dau2_mass{self.lep_syst}, "
                    f"bjet1_pt{self.jet_syst}, bjet1_eta, bjet1_phi, bjet1_mass{self.jet_syst}, "
                    f"bjet2_pt{self.jet_syst}, bjet2_eta, bjet2_phi, bjet2_mass{self.jet_syst}, "
                    "bjet1_btag, bjet1_btagCvB, bjet1_btagCvL, bjet1_hhbtag, "
                    "bjet2_btag, bjet2_btagCvB, bjet2_btagCvL, bjet2_hhbtag, "
                    f"vbfjet1_pt{self.jet_syst}, vbfjet1_eta, vbfjet1_phi, vbfjet1_mass{self.jet_syst}, "
                    f"vbfjet2_pt{self.jet_syst}, vbfjet2_eta, vbfjet2_phi, vbfjet2_mass{self.jet_syst}, "
                    "vbfjet1_btagCvB, vbfjet1_btagCvL, vbfjet1_hhbtag, "
                    "vbfjet2_btagCvB, vbfjet2_btagCvL, vbfjet2_hhbtag, "
                    f"Htt_svfit_pt{self.systs},Htt_svfit_eta{self.systs}, Htt_svfit_phi{self.systs}, Htt_svfit_mass{self.systs}, "
                    f"HH_kinfit_mass{self.systs}, HH_kinfit_chi2{self.systs}, "
                    f"PuppiMET{self.met_smear_tag}_pt{self.met_syst}, PuppiMET{self.met_smear_tag}_phi{self.met_syst})"
                    ).Define(branches[0], "dnn_output%s[0]" % self.systs)

        else:
            df = df.Define("dnn_output%s" % self.systs,
                    "HHDNN.get_dnn_outputs(pairType, hasBoostedAK8, event, "
                    "dau1_index, dau2_index, bjet1_JetIdx, bjet2_JetIdx, hasResolvedAK4, "
                    "VBFjet1_JetIdx, VBFjet2_JetIdx, "
                    f"Muon_pt{self.muon_syst}, Muon_eta, Muon_phi, Muon_mass{self.muon_syst},"
                    f"Electron_pt{self.electron_syst}, Electron_eta, Electron_phi, Electron_mass{self.electron_syst}, "
                    f"Tau_pt{self.tau_syst}, Tau_eta, Tau_phi, Tau_mass{self.tau_syst}, "
                    f"Jet_pt{self.jet_syst}, Jet_eta, Jet_phi, Jet_mass{self.jet_syst}, "
                    f"{p}tt_svfit_pt{self.systs}, {p}tt_svfit_eta{self.systs}, "
                    f"{p}tt_svfit_phi{self.systs}, {p}tt_svfit_mass{self.systs}, "
                    f"{p}{p}_kinfit_mass{self.systs}, {p}{p}_kinfit_chi2{self.systs}, "
                    f"PuppiMET{self.met_smear_tag}_pt{self.met_syst}, PuppiMET{self.met_smear_tag}_phi{self.met_syst}, "
                    "Jet_btagPNetB, Jet_btagPNetCvL, Jet_btagPNetCvB, Jet_HHbtag)"
                    ).Define(branches[0], "dnn_output%s[0]" % self.systs)

        return df, branches


def HHDNNInputRDF(**kwargs):
    """
    Returns the HH->bbtt or ZZ->bbtt DNN input.

    Lepton and jet systematics (used for pt and mass variables) can be modified using the parameters
    from :ref:`BaseModules_JetLepMetSyst`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHDNNInputRDF
            path: Tools.Tools.HHDNN
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isZZAnalysis: True

    """
    isZZAnalysis = kwargs.pop("isZZAnalysis", False)

    # print("### DEBUG 1 : isZZAnalysis = {}".format(isZZAnalysis))
    return lambda: HHDNNInputRDFProducer(isZZAnalysis=isZZAnalysis, **kwargs)


def HHDNNRDF(**kwargs):
    """
    Returns the HH->bbtt DNN or ZZ->bbtt output.

    Lepton and jet systematics (used for pt and mass variables) can be modified using the parameters
    from :ref:`BaseModules_JetLepMetSyst`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHDNNRDF
            path: Tools.Tools.HHDNN
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isZZAnalysis: True

    """
    isZZAnalysis = kwargs.pop("isZZAnalysis", False)

    # print("### DEBUG 2 : isZZAnalysis = {}".format(isZZAnalysis))
    return lambda: HHDNNRDFProducer(isZZAnalysis=isZZAnalysis, **kwargs)
