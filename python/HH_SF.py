import os
import envyaml
from copy import deepcopy as copy

from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root

ROOT = import_root()

btagCorrCfg = envyaml.EnvYAML('%s/src/Corrections/BTV/python/btagCorrectionsFiles.yaml' %
                                    os.environ['CMSSW_BASE'])


class HH_dauIdIsoSFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_dauIdIsoSFRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.tauId_algo = kwargs.pop("tauId_algo")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)
        systnames = kwargs.pop("systs", ["central"])

        if self.isMC:
            default_systs = ["muon_id", "muon_iso", "ele_reco", "ele_iso", "tau_vsjet", "tau_vse", "tau_vsmu"]
            # store systematics impact on SF only for the central production;
            # skip them for all systematic variated selections
            if not self.skip_unused_systs or self.systs == "":
                systnames += default_systs

            template = ["" for i in range(len(default_systs))]
            self.syst_names = []
            self.systs_local = []
            for name in systnames:
                if name == "central":
                    self.syst_names.append("")
                    self.systs_local.append(template)
                    continue
                try:
                    ind = default_systs.index(name)
                    for d in ["_up", "_down"]:
                        tmp = copy(template)
                        self.syst_names.append("_%s%s" % (name, d))
                        tmp[ind] = d
                        self.systs_local.append(tmp)
                except ValueError:
                    raise ValueError("Systematic %s not available" % name)

            if self.year <= 2018:
                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    double get_dauIdIso_sf(
                            int pairType, int dau1_index, int dau2_index, Vfloat Tau_pt,
                            Vfloat musf_id, Vfloat musf_reliso, Vfloat elesf, 
                            Vfloat Tau_sfVSjet_pt, Vfloat Tau_sfVSjet_dm,
                            Vfloat Tau_sfVSe, Vfloat Tau_sfVSmu) {
                        
                        if (dau1_index < 0 || dau2_index < 0) { return 1.0; }

                        double idAndIsoSF_leg1 = 1.;
                        if (pairType == 0 || pairType == 3 || pairType == 5) {
                            idAndIsoSF_leg1 = musf_id.at(dau1_index) * musf_reliso.at(dau1_index);
                        } else if (pairType == 1 || pairType == 4) {
                            idAndIsoSF_leg1 = elesf.at(dau1_index);
                        } else if (pairType == 2) {
                            if (Tau_pt[dau1_index] < 40)
                                idAndIsoSF_leg1 = Tau_sfVSjet_pt.at(dau1_index);
                            else idAndIsoSF_leg1 = Tau_sfVSjet_dm.at(dau1_index);

                            idAndIsoSF_leg1 *= Tau_sfVSe.at(dau1_index) *
                                Tau_sfVSmu.at(dau1_index);
                        }
                        double idAndIsoSF_leg2 = 1.;
                        if (pairType == 2) {
                            if (Tau_pt[dau1_index] > 40)
                                idAndIsoSF_leg2 = Tau_sfVSjet_dm.at(dau2_index);
                            else
                                idAndIsoSF_leg2 = Tau_sfVSjet_pt.at(dau2_index);
                            idAndIsoSF_leg2 *= Tau_sfVSe.at(dau2_index) *
                                               Tau_sfVSmu.at(dau2_index);
                        }
                        else if (pairType == 3) {
                            idAndIsoSF_leg2 = musf_id.at(dau2_index) * musf_reliso.at(dau2_index);
                        }
                        else if (pairType == 4 || pairType == 5) {
                            idAndIsoSF_leg2 = elesf.at(dau2_index);
                        }
                        return idAndIsoSF_leg1 * idAndIsoSF_leg2;
                    }
                """)

            else:
                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    double get_dauIdIso_sf(
                            int pairType, int dau1_index, int dau2_index,
                            Vfloat musf_id, Vfloat musf_reliso,
                            Vfloat elesf_reco, Vfloat elesf_iso,
                            Vfloat Tau_sfVSjet, Vfloat Tau_sfVSe,
                            Vfloat Tau_sfVSmu) {
                        
                        if (dau1_index < 0 || dau2_index < 0) { return 1.0; }

                        double idAndIsoSF_leg1 = 1.;
                        double idAndIsoSF_leg2 = 1.;
                        if (pairType == 0) {
                            idAndIsoSF_leg1 = musf_id.at(dau1_index) * musf_reliso.at(dau1_index);
                            idAndIsoSF_leg2 = Tau_sfVSjet.at(dau2_index) *
                                              Tau_sfVSe.at(dau2_index) *
                                              Tau_sfVSmu.at(dau2_index);
                        } else if (pairType == 1) {
                            idAndIsoSF_leg1 = elesf_iso.at(dau1_index) *
                                              elesf_reco.at(dau1_index);
                            idAndIsoSF_leg2 = Tau_sfVSjet.at(dau2_index) *
                                              Tau_sfVSe.at(dau2_index) *
                                              Tau_sfVSmu.at(dau2_index);
                        } else if (pairType == 2) {
                            idAndIsoSF_leg1 = Tau_sfVSjet.at(dau1_index) *
                                              Tau_sfVSe.at(dau1_index) *
                                              Tau_sfVSmu.at(dau1_index);
                            idAndIsoSF_leg2 = Tau_sfVSjet.at(dau2_index) *
                                              Tau_sfVSe.at(dau2_index) *
                                              Tau_sfVSmu.at(dau2_index);
                        } else if (pairType == 3) {
                            idAndIsoSF_leg1 = musf_id.at(dau1_index) * musf_reliso.at(dau1_index);
                            idAndIsoSF_leg2 = musf_id.at(dau2_index) * musf_reliso.at(dau2_index);
                        } else if (pairType == 4) {
                            idAndIsoSF_leg1 = elesf_iso.at(dau1_index) *
                                              elesf_reco.at(dau1_index);
                            idAndIsoSF_leg2 = elesf_iso.at(dau2_index) *
                                              elesf_reco.at(dau2_index);
                        } else if (pairType == 5) {
                            idAndIsoSF_leg1 = musf_id.at(dau1_index) * musf_reliso.at(dau1_index);
                            idAndIsoSF_leg2 = elesf_iso.at(dau2_index) *
                                              elesf_reco.at(dau2_index);
                        }
                        return idAndIsoSF_leg1 * idAndIsoSF_leg2;
                    }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []
        
        # This part checking the existence of the electron reco sfs is only temporary
        # for backcompartibility with the miniProd_24_11 preprocessed pNTuples. Once we
        # move to a new production, it will be removed. (This is out of extereme safety
        # but should actuually never be useful as this is called only at PreprocessRDF,
        # and never later on. But better safe then sorry). [JONA]
        all_branches = df.GetColumnNames()
        elesf_RecoAbove20 = "elesf_RecoAbove20"
        if elesf_RecoAbove20 not in all_branches:
            df = df.Define("elesf_RecoAbove20", "std::vector<float>(elesf_wp80iso.size(), 1.)")
            df = df.Define("elesf_RecoAbove20_up", "std::vector<float>(elesf_wp80iso.size(), 1.)")
            df = df.Define("elesf_RecoAbove20_down", "std::vector<float>(elesf_wp80iso.size(), 1.)")

        branches = []
        for name, syst_dir in zip(self.syst_names, self.systs_local):
            if self.year <= 2018:
                df = df.Define("idAndIsoAndFakeSF%s" % name,
                    "get_dauIdIso_sf(pairType, dau1_index, dau2_index, Tau_pt{0}, "
                        "musf_tight_id{1[0]}, musf_tight_reliso{1[1]}, elesf_wp80iso{1[2]}, "
                        "Tau_sf{2}VSjet_pt_binned_Medium{1[3]}, "
                        "Tau_sf{2}VSjet_dm_binned_Medium{1[3]}, "
                        "Tau_sf{2}VSe_VVLoose{1[4]},"
                        "Tau_sf{2}VSmu_VLoose{1[5]})".format(self.tau_syst, 
                                                             syst_dir,
                                                             self.tauId_algo[2:]))

            else:
                df = df.Define("idAndIsoAndFakeSF%s" % name,
                    "get_dauIdIso_sf(pairType, dau1_index, dau2_index, "
                        "musf_tight_id{0[0]}, musf_tight_iso{0[1]}, "
                        "elesf_RecoAbove20{0[2]}, elesf_wp80iso{0[3]}, "
                        "Tau_sf{1}VSjet_dm_binned_Medium{0[4]}, "
                        "Tau_sf{1}VSe_VVLoose{0[5]},"
                        "Tau_sf{1}VSmu_VLoose{0[6]})".format(syst_dir,
                                                             self.tauId_algo[2:]))

            branches.append("idAndIsoAndFakeSF%s" % name)

        return df, branches


def HH_dauIdIsoSFRDF(**kwargs):
    """
    Returns the ID and Isolation SF applied to the two leptons with the desired systematics.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`, :ref:`Electron_eleSFRDF`,
    :ref:`Muon_muSFRDF`, :ref:`Tau_tauSFRDF`

    :param systs: Systematics to be considered. Default: [``central``, ``muon_id``, ``muon_iso``,
        ``ele_reco``, ``ele_iso``, ``tau_vsjet``, ``tau_vse``, ``tau_vsmu``].
    :type systs: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HH_dauIdIsoSFRDF
            path: Tools.Tools.HH_SF
            parameters:
                isMC: self.dataset.process.isMC
                systs: [central, ...]

    """
    return lambda: HH_dauIdIsoSFRDFProducer(**kwargs)


class HH_btagSFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_btagSFRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.btag_algo_cut = kwargs.pop("btag_algo_cut", "M")
        self.bypass_HHBTag = kwargs.pop("bypass_HHBTag", False)
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        if self.isMC:
            ROOT.gInterpreter.Declare("""
                using Vdouble = const ROOT::RVec<double>&;
                double get_btag_weight(Vdouble btag_sf, int bjet1_idx, int bjet2_idx) {
                    if (bjet1_idx >= 0 && bjet2_idx >= 0) {
                        return btag_sf.at(bjet1_idx) * btag_sf.at(bjet2_idx);
                    }
                    else { return 1.0; }
                }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        # btag hard cut SF
        if self.bypass_HHBTag:
            for syst_name in ["", "_up", "_down"]:
                # store systematics impact on SF only for the central production;
                # skip them for all systematic variated selections
                if self.skip_unused_systs and self.systs != "" and syst_name != "":
                    continue

                df = df.Define(f"bTagweightReshape{syst_name}",
                               f"get_btag_weight(btagsf_wp{self.btag_algo_cut}{syst_name}, "
                               "bjet1_JetIdx, bjet2_JetIdx)")

                branches.append(f"bTagweightReshape{syst_name}")

        # btag shape SF
        else:
            systematics2store = []
            for syst_name, syst in btagCorrCfg["btag_shape_systematics"]:
                # skip all the cases for which the systematic variation would not actually be used
                if self.skip_unused_systs and syst != "central" and self.systs != "":
                    continue
                systematics2store.append([syst_name, syst])

            for syst_name, syst in btagCorrCfg["jes_shape_systematics"]:
                # skip all the cases for which the systematic variation would not actually be used
                if self.skip_unused_systs and syst_name != self.jet_syst:
                    continue
                systematics2store.append([syst_name, syst])

            for syst_name, syst in systematics2store:
                df = df.Define(f"bTagweightReshape{syst_name}",
                               f"get_btag_weight(btagsf_shape{syst_name}, "
                               "bjet1_JetIdx, bjet2_JetIdx)")

                branches.append(f"bTagweightReshape{syst_name}")

        return df, branches


def HH_btagSFRDF(**kwargs):
    """
    THIS MODULE IS THE OLD AND BUGGED IMPLEMENTATION OF THE bTagweightReshape WEIGHT
    IT IS KEPT FOR THE MOMENT TO KEEP FULL COMPATIBILITY WITH miniProd_24_11
    IT WILL BE REMOVED AS SOON AS WE WILL OFFICIALLY MOVE AWAY FROM THIS PRODUCTION

    Module to obtain btagging deepJet SFs with their uncertainties
    for the HH->bbtautau analysis.

    :param btag_algo_cut: working point to be applied for the btag algorithm
    :type btag_algo_cut: string

    :param bypass_HHBTag: apply HHBTag algorithm or bypass it
    :type bypass_HHBTag: bool

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHbtag_SFRDF
            path: Tools.Tools.HH_SF
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                btag_algo_cut: self.config.btag_algo_cut
                bypass_HHBTag: False/True

    """
    return lambda: HH_btagSFRDFProducer(**kwargs)


class HH_btagSFRDFProducer_TEMPORARY(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_btagSFRDFProducer_TEMPORARY, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        if self.isMC:
            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using Vint = const ROOT::RVec<int>&;
                float get_btag_reshape_weight(
                    int pairType, int dau1_index, int dau2_index,
                    Vfloat tau_pt, Vfloat tau_eta, Vfloat tau_phi, Vfloat tau_mass,
                    Vfloat jet_pt, Vfloat jet_eta, Vfloat jet_phi, Vfloat jet_mass,
                    Vint jet_jetId, Vfloat btag_shape_sf)
                {
                    double sf = 1.0;
                    for (size_t ijet = 0; ijet < jet_pt.size(); ijet++) {
                        if (jet_jetId[ijet] != 6) { continue; }
                        if (jet_pt[ijet] < 20) { continue; }
                        if (fabs(jet_eta[ijet]) >= 2.5) { continue; }

                        auto jet_tlv = TLorentzVector();
                        jet_tlv.SetPtEtaPhiM(jet_pt[ijet], jet_eta[ijet], jet_phi[ijet], jet_mass[ijet]);
                        if (pairType == 0 || pairType == 1) {
                            auto dau2_tlv = TLorentzVector();
                            dau2_tlv.SetPtEtaPhiM(tau_pt[dau2_index], tau_eta[dau2_index], tau_phi[dau2_index], tau_mass[dau2_index]);
                            if (jet_tlv.DeltaR(dau2_tlv) < 0.5) { continue; }
                        }
                        else if (pairType == 2) {
                            auto dau1_tlv = TLorentzVector();
                            auto dau2_tlv = TLorentzVector();
                            dau1_tlv.SetPtEtaPhiM(tau_pt[dau1_index], tau_eta[dau1_index], tau_phi[dau1_index], tau_mass[dau1_index]);
                            dau2_tlv.SetPtEtaPhiM(tau_pt[dau2_index], tau_eta[dau2_index], tau_phi[dau2_index], tau_mass[dau2_index]);
                            if (jet_tlv.DeltaR(dau1_tlv) < 0.5 || jet_tlv.DeltaR(dau2_tlv) < 0.5) { continue; }
                        }

                        sf *= btag_shape_sf[ijet];

                    }

                    return sf;
                }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        # btag shape SF
        systematics2store = []
        for syst_name, syst in btagCorrCfg["btag_shape_systematics"]:
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs and syst != "central" and self.systs != "":
                continue
            systematics2store.append([syst_name, syst])

        for syst_name, syst in btagCorrCfg["jes_shape_systematics"]:
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs and syst_name != self.jet_syst:
                continue
            systematics2store.append([syst_name, syst])

        for syst_name, syst in systematics2store:
            df = df.Define(f"NEWbTagweightReshape{syst_name}",
                           "get_btag_reshape_weight("
                           "pairType, dau1_index, dau2_index, "
                          f"Tau_pt{self.tau_syst}, Tau_eta, Tau_phi, Tau_mass{self.tau_syst}, "
                          f"Jet_pt{self.jet_syst}, Jet_eta, Jet_phi, Jet_mass{self.jet_syst}, "
                          f"Jet_jetId, btagsf_shape{syst_name})")

            branches.append(f"NEWbTagweightReshape{syst_name}")

        return df, branches


def HH_btagSFRDF_TEMPORARY(**kwargs):
    """
    TEMPORARY MODULE TO BE USED ONLY IN THE CATEGORIZATION STEP OF THE miniProd_24_11
    WHICH FIXES THE BTAG WEIGHT RESHAPING AS RECOMMENDED BY THE BTV POG
    https://btv-wiki.docs.cern.ch/ScaleFactors/

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHbtag_SFRDF
            path: Tools.Tools.HH_SF
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                skipUnusedSysts: True/False

    """
    return lambda: HH_btagSFRDFProducer_TEMPORARY(**kwargs)


class HH_pNetSFRDFProduced(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HH_pNetSFRDFProduced, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        # Check whether HH-like, TT-like, DY-like or other for pNet SF:
        #  - HH-like : ggHH, VBFHH, ZH, WH, ttH, ggH, qqH, ttWH, ttZH
        #  - TT-like : TT fullyLep, semiLep, fullyHad
        #  - DY_like : all DY samples
        #  - other   : all other samples
        dsetName = kwargs.pop("dataset")
        if "HH" in dsetName or "Hto2B" in dsetName or "WH" in dsetName or "ZH" in dsetName:
            sampleType = "HHlike"
        elif "TTto" in dsetName:
            sampleType = "TTlike"
        elif "DY" in dsetName:
            sampleType = "DYlike"
        else:
            sampleType = "other"
        self.sampleType = sampleType

        if self.isMC:
            base = "{}/{}/src/Tools/Tools".format(os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/PNetSFInterface.h".format(base))

            ROOT.gInterpreter.Declare("""
                auto PNetAK8SF = PNetSFInterface("%s");
            """ % (self.year))

    def run(self, df):
        # In case of data: no SFs
        if not self.isMC:
            return df, []

        # In case of MC: compute SFs based on jet pT, sample type and gen-matches variables
        df = df.Define("fatjet_pNet_SF_vec", "PNetAK8SF.getSFvec(FatJet_pt{1}, fatjet_JetIdx, "
                                             " genAk8_Zbb_matches, genAk8_Hbb_matches, "
                                             " \"{0}\")".format(self.sampleType, self.jet_syst))
        df = df.Define("fatjet_pNet_HP_SF",      "fatjet_pNet_SF_vec[0]")
        df = df.Define("fatjet_pNet_HP_SF_up",   "fatjet_pNet_SF_vec[1]")
        df = df.Define("fatjet_pNet_HP_SF_down", "fatjet_pNet_SF_vec[2]")
        df = df.Define("fatjet_pNet_MP_SF",      "fatjet_pNet_SF_vec[3]")
        df = df.Define("fatjet_pNet_MP_SF_up",   "fatjet_pNet_SF_vec[4]")
        df = df.Define("fatjet_pNet_MP_SF_down", "fatjet_pNet_SF_vec[5]")
        df = df.Define("fatjet_pNet_LP_SF",      "fatjet_pNet_SF_vec[6]")
        df = df.Define("fatjet_pNet_LP_SF_up",   "fatjet_pNet_SF_vec[7]")
        df = df.Define("fatjet_pNet_LP_SF_down", "fatjet_pNet_SF_vec[8]")

        branches = ["fatjet_pNet_HP_SF", "fatjet_pNet_HP_SF_up", "fatjet_pNet_HP_SF_down",
                    "fatjet_pNet_MP_SF", "fatjet_pNet_MP_SF_up", "fatjet_pNet_MP_SF_down",
                    "fatjet_pNet_LP_SF", "fatjet_pNet_LP_SF_up", "fatjet_pNet_LP_SF_down"]

        # store systematics impact on SF only for the central production;
        # skip them for all systematic variated selections
        if self.skip_unused_systs and self.systs != "":
            branches = ["fatjet_pNet_HP_SF", "fatjet_pNet_MP_SF", "fatjet_pNet_LP_SF"]

        return df, branches


def HH_pNetSFRDF(**kwargs):
    """
    Module to obtain pNet SFs for AK8 jets depending on sample type,
    FataJet pT and gen-matching to a H/Z->bb resonance

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HH_pNetSFRDF
            path: Tools.Tools.HH_SF
            parameters:
                isMC: self.dataset.process.isMC
                dataset: self.dataset.name
                year: self.config.year

    """
    return lambda: HH_pNetSFRDFProduced(**kwargs)

#################################################################################
# HH_PUjetID_SFRDF and HH_PUjetID_SFRDFProducer are Run-2 specific
# it produces the PU jet ID SFs recommended by JME for Run-2 PF jets
from Corrections.JME.PUjetID_SF import PUjetID_SFRDFProducer

class HH_PUjetID_SFRDFProducer(PUjetID_SFRDFProducer):
    def __init__(self, year, *args, **kwargs):
        super(HH_PUjetID_SFRDFProducer, self).__init__(year, *args, **kwargs)
        self.lep_pt = "{dau1_pt%s, dau2_pt%s}" % (self.systs, self.systs)
        self.lep_eta = "{dau1_eta, dau2_eta}"
        self.lep_phi = "{dau1_phi, dau2_phi}"
        self.lep_mass = "{dau1_mass%s, dau2_mass%s}" % (self.systs, self.systs)


def HH_PUjetID_SFRDF(**kwargs):
    """
    Module to compute PU Jet Id scale factors for the HH->bbtautau analysis.
    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HH_PUjetID_SFRDF
            path: Tools.Tools.HH_SF
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux("isPreVFP", False)

    """
    year = kwargs.pop("year")
    return lambda: HH_PUjetID_SFRDFProducer(year, **kwargs)
