import os

from analysis_tools.utils import import_root

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from Tools.Tools.utils_trigger import get_trigger_requests
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()

class HHLeptonRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHLeptonRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.runPeriod = kwargs.pop("runPeriod")
        self.pairType_filter = kwargs.pop("pairType_filter", "signals")
        self.tauId_algo = kwargs.pop("tauId_algo")
        self.tauId_algo_wps = kwargs.pop("tauId_algo_wps")
        self.applyOflnIso = kwargs.pop("applyOflnIso", True)

        self.EEleakVeto = "false"
        if self.year == 2022 and self.runPeriod == "postEE":
            self.EEleakVeto = "true"

        if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libToolsTools.so")

        base = "{}/{}/src/Tools/Tools".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/HHLeptonInterface.h".format(base))

        ROOT.gInterpreter.Declare("""
            auto HHLepton = HHLeptonInterface(%s, %s, %s, %s, %s, %s);
        """ % (self.tauId_algo_wps["vsjet"]["VVVLoose"],
               self.tauId_algo_wps["vse"]["VLoose"],
               self.tauId_algo_wps["vse"]["VVLoose"],
               self.tauId_algo_wps["vsmu"]["Tight"],
               self.tauId_algo_wps["vsmu"]["VLoose"],
               "true" if self.applyOflnIso else "false"))

    def run(self, df):
        variables = ["pairType", "dau1_index", "dau2_index", "isOS"]

        all_branches = df.GetColumnNames()
        Electron_mvaIso_WP80 = "Electron_mvaIso_WP80"
        if Electron_mvaIso_WP80 not in all_branches:
            Electron_mvaIso_WP80 = "Electron_mvaFall17V2Iso_WP80"
        Electron_mvaIso_WP90 = "Electron_mvaIso_WP90"
        if Electron_mvaIso_WP90 not in all_branches:
            Electron_mvaIso_WP90 = "Electron_mvaFall17V2Iso_WP90"
        Electron_mvaNoIso_WP90 = "Electron_mvaNoIso_WP90"
        if Electron_mvaNoIso_WP90 not in all_branches:
            Electron_mvaNoIso_WP90 = "Electron_mvaFall17V2noIso_WP90"

        df = df.Define("hh_lepton_pairs_results", "HHLepton.get_dau_indexes("
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
            "Muon_pfRelIso04_all, Muon_dxy, Muon_dz, Muon_mediumId, Muon_tightId, Muon_charge, "
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
            "{3}, {4}, {5}, Electron_pfRelIso03_all,  "
            "Electron_dxy, Electron_dz, Electron_charge, Electron_mvaIso, "
            "Electron_seediEtaOriX, Electron_seediPhiOriY, {8},"
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
            "Tau_{6}VSmu, Tau_{6}VSe, "
            "Tau_{6}VSjet, Tau_raw{7}VSjet, "
            "Tau_dz, Tau_decayMode, Tau_charge"
            ")".format(self.muon_syst, self.electron_syst, self.tau_syst,
            Electron_mvaIso_WP80, Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
            self.tauId_algo, self.tauId_algo[2:], self.EEleakVeto))

        for var in variables:
            df = df.Define(var, "hh_lepton_pairs_results.%s" % var)

        # Parse pairType_filter looking for additional channels...
        filters = self.pairType_filter.split(",")
        for filt in filters:
            if filt not in ["all", "signals", "0", "1", "2", "3", "4", "5"]:
                raise ValueError("pairType_filter with value {} is not valid".format(filt))
            
        add_chnls = [ filt for filt in filters if filt == "0" or filt == "1" or
                                                  filt == "2" or filt == "3" or
                                                  filt == "4" or filt == "5" ]

        # ...and compose final selection string
        selection_string = ""
        if "signals" in filters:
            selection_string = "(pairType == 0 || pairType == 1 || pairType == 2)"
        elif "all" in filters:
            selection_string = "(pairType >= -1)"

        for add_chnl in add_chnls:
            if selection_string == "":
                selection_string += "(pairType == {})".format(add_chnl)
            else:
                selection_string += " || (pairType == {})".format(add_chnl)
        # Check final selectin string...
        if selection_string == "":
            raise ValueError("No pairType_filter requested! Please set at least one option among: "
                             "all, signals, or specific channels numbers [0, 1, 2, 3, 4, 5]")

        # ...and filter the RDF with with
        df = df.Filter(selection_string)

        return df, variables

def HHLeptonRDF(**kwargs):
    """
    Returns the index of the two selected taus + several of their variables not affected by systematics.

    Lepton systematics (used for pt and mass variables) can be modified using the parameters from 
    :ref:`BaseModules_JetLepMetSyst`.

    :param NanoAODv: version of the NanoAOD production
    :type NanoAODv: str

    :param tauId_algo: Tau ID algorthm to be used
    :type tauId_algo: string

    :param tauId_algo_wps: Tau ID algorthm to be used
    :type tauId_algo_wps: dict

    :param pairType_filter: whether to filter out output events if they don't have 2 lepton candidates
    :type pairType_filter: bool

    :param applyOflnIso: whether to apply the offline Id&Iso selections
    :type applyOflnIso: bool

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHLeptonRDF
            path: Tools.Tools.HHLepton
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                tauId_algo: self.config.tauId_algo
                tauId_algo_wps: self.config.tauId_algo_wps
                pairType_filter: True
                applyOflnIso: True

    """
    return lambda: HHLeptonRDFProducer(**kwargs)



class HHLeptonVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHLeptonVarRDFProducer, self).__init__(*args, **kwargs)
        self.tauId_algo = kwargs.pop("tauId_algo")

        if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libToolsTools.so")

        base = "{}/{}/src/Tools/Tools".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        
        if not ROOT.gInterpreter.IsLoaded("{}/interface/HHUtils.h".format(base)):
            ROOT.gROOT.ProcessLine(".L {}/interface/HHUtils.h".format(base))

        ROOT.gInterpreter.Declare("""
            using Vfloat = const ROOT::RVec<float>&;
            using VInt = const ROOT::RVec<int>&;
            float DeltaRtautau(
                    int pairType, int dau1_index, int dau2_index,
                    Vfloat muon_pt, Vfloat muon_eta, Vfloat muon_phi, Vfloat muon_mass,
                    Vfloat ele_pt, Vfloat ele_eta, Vfloat ele_phi, Vfloat ele_mass,
                    Vfloat tau_pt, Vfloat tau_eta, Vfloat tau_phi, Vfloat tau_mass
            )
            {
                std::pair<TLorentzVector, TLorentzVector> leps;
                
                if (pairType == 0) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      muon_pt, muon_eta, muon_phi, muon_mass,
                                      tau_pt, tau_eta, tau_phi, tau_mass);
                } else if (pairType == 1) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      ele_pt, ele_eta, ele_phi, ele_mass,
                                      tau_pt, tau_eta, tau_phi, tau_mass);
                } else if (pairType == 2) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      tau_pt, tau_eta, tau_phi, tau_mass,
                                      tau_pt, tau_eta, tau_phi, tau_mass);
                } else if (pairType == 3) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      muon_pt, muon_eta, muon_phi, muon_mass,
                                      muon_pt, muon_eta, muon_phi, muon_mass);
                } else if (pairType == 4) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      ele_pt, ele_eta, ele_phi, ele_mass,
                                      ele_pt, ele_eta, ele_phi, ele_mass);
                } else if (pairType == 5) {
                    leps = GetLepsTLV(dau1_index, dau2_index,
                                      muon_pt, muon_eta, muon_phi, muon_mass,
                                      ele_pt, ele_eta, ele_phi, ele_mass);
                } else {
                    return -999.;
                }

                return leps.first.DeltaR(leps.second);
            }
        """)

    def run(self, df):
        variables_str = ("dau1_pt{0},dau1_eta,dau1_phi,dau1_mass{0},dau1_charge,dau1_DM,"
                         "dau1_tauIdVSe,dau1_tauIdVSmu,dau1_tauIdVSjet,"
                         "dau2_pt{0},dau2_eta,dau2_phi,dau2_mass{0},dau2_charge,dau2_DM,"
                         "dau2_tauIdVSe,dau2_tauIdVSmu,dau2_tauIdVSjet"
                         "".format(self.lep_syst))
        variables = variables_str.split(",")

        df = df.Define("lepsfeatures%s" % self.lep_syst, "GetLeps("
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0},  Muon_charge, "
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1},  Electron_charge, "
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, Tau_charge, Tau_decayMode, "
            "Tau_{3}VSe, Tau_{3}VSmu, Tau_{3}VSjet, "
            "dau1_index, dau2_index, pairType)".format(self.muon_syst, self.electron_syst, self.tau_syst, self.tauId_algo))

        features = []
        for var in variables:
            varStructName = var.replace(self.lep_syst,'')
            df = df.Define(var, f"lepsfeatures{self.lep_syst}.{varStructName}")
            features.append(var)

        df = df.Define("deltaRtautau{0}".format(self.lep_syst), "DeltaRtautau("
            "pairType, dau1_index, dau2_index, "
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2})"
            "".format(self.muon_syst, self.electron_syst, self.tau_syst))

        features.append("deltaRtautau{0}".format(self.lep_syst))

        # store miscellanea variables only for the central production;
        # no need to store all of them for all systematic variations
        if self.systs == "":
            miscellanea = ["dau1_dxy","dau1_dxyErr","dau1_dz","dau1_dzErr","dau1_mvaIso",
                           "dau1_mvaIso_WP80","dau1_mvaIso_WP90",
                           "dau1_mvaNoIso","dau1_mvaNoIso_WP80","dau1_mvaNoIso_WP90","dau1_mediumId",
                           "dau1_tightId","dau1_pfRelIso04_all","dau1_eleIdx","dau1_muIdx",
                           "dau1_genPartFlav","dau1_genPartIdx","dau1_idAntiMu","dau1_idDecayModeNewDMs",
                           "dau1_nSVs","dau1_rawDeepTau2018v2p5VSe","dau1_rawDeepTau2018v2p5VSjet",
                           "dau1_rawDeepTau2018v2p5VSmu",
                           "dau2_dxy","dau2_dxyErr","dau2_dz","dau2_dzErr","dau2_mvaIso",
                           "dau2_mvaIso_WP80","dau2_mvaIso_WP90",
                           "dau2_mvaNoIso","dau2_mvaNoIso_WP80","dau2_mvaNoIso_WP90","dau2_mediumId",
                           "dau2_tightId","dau2_pfRelIso04_all","dau2_eleIdx","dau2_muIdx",
                           "dau2_genPartFlav","dau2_genPartIdx","dau2_idAntiMu","dau2_idDecayModeNewDMs",
                           "dau2_nSVs","dau2_rawDeepTau2018v2p5VSe","dau2_rawDeepTau2018v2p5VSjet",
                           "dau2_rawDeepTau2018v2p5VSmu"]

            Tau_genPartFlav = "Tau_genPartFlav"
            Tau_genPartIdx = "Tau_genPartIdx"
            if not self.isMC:
                # replace Tau_genPartFlav, Tau_genPartIdx with dummyTauVecF for data
                df = df.Define("dummyTauVecF", "std::vector<float>(Tau_pt.size(), -999.)")
                Tau_genPartFlav = "dummyTauVecF"
                Tau_genPartIdx = "dummyTauVecF"

            df = df.Define("lepsmiscellanea", "GetLepsMisc("
                "Muon_dxy, Muon_dxyErr, Muon_dz, Muon_dzErr, Muon_mediumId, Muon_pfRelIso04_all, Muon_tightId, "
                "Electron_dxy, Electron_dxyErr, Electron_dz, Electron_dzErr, Electron_mvaIso, Electron_mvaIso_WP80, Electron_mvaIso_WP90, "
                "Electron_mvaNoIso, Electron_mvaNoIso_WP80, Electron_mvaNoIso_WP90, "
                f"Tau_dxy, Tau_dz, Tau_eleIdx, {Tau_genPartFlav}, {Tau_genPartIdx}, Tau_idAntiMu, Tau_idDecayModeNewDMs, "
                "Tau_muIdx, Tau_nSVs, Tau_rawDeepTau2018v2p5VSe, Tau_rawDeepTau2018v2p5VSjet, Tau_rawDeepTau2018v2p5VSmu, "
                "dau1_index, dau2_index, pairType)")


            for misc in miscellanea:
                df = df.Define(misc, f"lepsmiscellanea.{misc}")
                features.append(misc)

        return df, features


def HHLeptonVarRDF(**kwargs):
    """
    Returns the pt and mass of the selected taus possibly including systematics

    Lepton systematics (used for pt and mass variables) can be modified using the parameters from 
    :ref:`BaseModules_JetLepMetSyst`.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHLeptonVarRDF
            path: Tools.Tools.HHLepton
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: HHLeptonVarRDFProducer(**kwargs)
