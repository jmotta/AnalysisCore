#include "Tools/Tools/interface/HHUtils.h"

//------------------------------------------------------------------------
// stuctures and functions needed for trigger checking and matching
bool match_trigger_object(float off_eta, float off_phi, int obj_id,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<int> bits, std::vector<int> addbits = (std::vector<int>) {}, int off_id = 0)
{
    for (size_t iobj = 0; iobj < TrigObj_id.size(); iobj++) {
        if (TrigObj_id[iobj] != obj_id) continue;
        auto const dPhi(std::abs(reco::deltaPhi(off_phi, TrigObj_phi[iobj])));
        auto const dEta(std::abs(off_eta - TrigObj_eta[iobj]));
        auto const delR2(dPhi * dPhi + dEta * dEta);
        if (delR2 > 0.5 * 0.5)
            continue;
        bool matched_bits = true;
        for (auto & bit : bits) {
            if ((TrigObj_filterBits[iobj] & (1<<bit)) == 0) {
                matched_bits = false;
                break;
            }
        }

        for (auto & bit : addbits) {
            if (obj_id != off_id) continue;
            if ((TrigObj_filterBits[iobj] & (1<<bit)) == 0) {
                matched_bits = false;
                break;
            }
        }

        if (!matched_bits)
            continue;

        return true;
    }
    return false;
}

// This version of function is used for single- & cross-lepton triggers
bool pass_trigger(std::vector<TLorentzVector> off_objs, 
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
{
    for (auto &trigger: triggers) {
        if (!trigger.pass)
            continue;

        int passObjs = 0;
        int totObjs = int( trigger.id.size() );

        for (int idx = 0; idx < totObjs; ++idx) {
            if ((off_objs.at(idx).Pt() < trigger.pt.at(idx) || 
                  abs(off_objs.at(idx).Eta()) > trigger.eta.at(idx)))
                continue;

            if (!match_trigger_object(off_objs.at(idx).Eta(), off_objs.at(idx).Phi(), trigger.id.at(idx),
                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(idx)))
                continue;

            passObjs += 1;
        }

        if (passObjs == totObjs &&
            (isMC || dataset.find(trigger.dataset) != std::string::npos) )
            return true;
    }
    return false;
}

// this is version for offline objects
bool pass_trigger(
    std::vector<TLorentzVector> off_objs, std::vector<int> off_ids,
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
{
    // combine off_objs and off_ids
    std::vector<std::pair<TLorentzVector, int>> combined;
    for (size_t i = 0; i < off_objs.size(); ++i) {
        combined.emplace_back(off_objs[i], off_ids[i]);
    }

    // sort both depending on off_objs Pt()
    std::sort(combined.begin(), combined.end(), [](const std::pair<TLorentzVector, int>& a, const std::pair<TLorentzVector, int>& b) {
        return a.first.Pt() > b.first.Pt();
    });

    // fill back the vectors now both ordered
    for (size_t i = 0; i < combined.size(); ++i) {
        off_objs[i] = combined[i].first;
        off_ids[i] = combined[i].second;
    }

    for (auto &trigger: triggers) {
        if (!trigger.pass)
            continue;

        int passObjs = 0;
        int totObjs = int( trigger.id.size() );

        for (int idx = 0; idx < totObjs; ++idx) {
            if ((off_objs.at(idx).Pt() < trigger.pt.at(idx) || 
                 abs(off_objs.at(idx).Eta()) > trigger.eta.at(idx)))
                continue;

            if (!match_trigger_object(off_objs.at(idx).Eta(), off_objs.at(idx).Phi(), trigger.id.at(idx),
                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(idx),
                trigger.addbits.at(idx), off_ids.at(idx)))
                continue;

            passObjs += 1;
        }

        if (passObjs == totObjs &&
            (isMC || dataset.find(trigger.dataset) != std::string::npos) )
            return true;
    }
    return false;
}


// // This version of function is used for jet triggers where additional info is necessary
// trigger_hlt_matched_output pass_trigger(
//     bool applyOfln, std::vector<TLorentzVector> mandatory_objs, std::vector<TLorentzVector> optional_objs, 
//     std::vector<trig_req> triggers, iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
// {
//     trigger_hlt_matched_output trig_out {false, 0};
//     for (auto &trigger: triggers) {
//         if (!trigger.pass)
//             continue;

//         int passMandObjs = 0;
//         int totObjs = int( trigger.id.size() );
//         int totMandObjs = (int) mandatory_objs.size();
//         std::vector<bool> trigObjPassed;
//         trigObjPassed.resize(totObjs);

//         for (int idx = 0; idx < totMandObjs; ++idx) {
//           for (int jdx = 0; jdx < totObjs; jdx++) {
//             if (trigObjPassed[jdx])
//                 continue;

//             if (applyOfln &&
//                  (mandatory_objs.at(idx).Pt() < trigger.pt.at(jdx) || 
//                   abs(mandatory_objs.at(idx).Eta()) > trigger.eta.at(jdx)))
//                 continue;

//             std::vector<int> all_bits;
//             all_bits.insert(all_bits.end(), trigger.bits.at(jdx).begin(), trigger.bits.at(jdx).end());
//             if (trigger.additional_bits_mand_objs.size())
//               all_bits.insert(all_bits.end(), trigger.additional_bits_mand_objs.at(jdx).begin(), trigger.additional_bits_mand_objs.at(jdx).end());
//             if (!match_trigger_object(mandatory_objs.at(idx).Eta(), mandatory_objs.at(idx).Phi(), trigger.id.at(jdx),
//                 TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, all_bits))
//                 continue;

//             trigObjPassed[jdx] = true;
//             trig_out.HLTMatchedJets += (1 << idx);
//             passMandObjs += 1;
//             break;
//           }
//         }

//         if (passMandObjs != totMandObjs)
//           continue;

//         int passOptObjs = 0;
//         int totOptObjs = (int) optional_objs.size();

//         for (int idx = 0; idx < totOptObjs; ++idx) {
//           for (int jdx = 0; jdx < totObjs; jdx++) {
//             if (trigObjPassed[jdx])
//                 continue;

//             if (applyOfln &&
//                  (optional_objs.at(idx).Pt() < trigger.pt.at(jdx) || 
//                   abs(optional_objs.at(idx).Eta()) > trigger.eta.at(jdx)))
//                 continue;

//             if (!match_trigger_object(optional_objs.at(idx).Eta(), optional_objs.at(idx).Phi(), trigger.id.at(jdx),
//                 TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(jdx)))
//                 continue;

//             trigObjPassed[jdx] = true;
//             trig_out.HLTMatchedJets += (1 << (totMandObjs + idx));
//             passOptObjs += 1;
//             break;
//           }
//         }

//         if (passMandObjs + passOptObjs == totObjs) {
//           trig_out.trigger_passed = true;
//           return trig_out;
//         }
//     }
//     return trig_out;
// }


//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHLeptonInterface

// pairSorting for MVA isolations: higher score --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortMVA (const tau_pair& pA, const tau_pair& pB)
{
    float isoA = pA.iso1;
    float isoB = pB.iso1;
    if (isoA > isoB) return true;
    else if (isoA < isoB) return false;

    float ptA = pA.pt1;
    float ptB = pB.pt1;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    isoA = pA.iso2;
    isoB = pB.iso2;
    if (isoA > isoB) return true;
    else if (isoA < isoB) return false;

    ptA = pA.pt2;
    ptB = pB.pt2;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    // should be never here..
    return false;
}

// pairSorting for Raw isolations: lower iso value --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortRawIso (const tau_pair& pA, const tau_pair& pB)
{
    float isoA = pA.iso1;
    float isoB = pB.iso1;
    if (isoA < isoB) return true;
    else if (isoA > isoB) return false;

    float ptA = pA.pt1;
    float ptB = pB.pt1;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    isoA = pA.iso2;
    isoB = pB.iso2;
    if (isoA < isoB) return true;
    else if (isoA > isoB) return false;

    ptA = pA.pt2;
    ptB = pB.pt2;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    // should be never here..
    return false;
}

// pairSorting for Lep+Tauh:
//  - leg1 (lepton): lower iso value --> more isolated
//  - leg2 (tauh)  : higher score    --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortHybrid (const tau_pair& pA, const tau_pair& pB)
{
    float isoA = pA.iso1;
    float isoB = pB.iso1;
    if (isoA < isoB) return true;
    else if (isoA > isoB) return false;

    float ptA = pA.pt1;
    float ptB = pB.pt1;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    isoA = pA.iso2;
    isoB = pB.iso2;
    if (isoA > isoB) return true;
    else if (isoA < isoB) return false;

    ptA = pA.pt2;
    ptB = pB.pt2;
    if (ptA > ptB) return true;
    else if (ptA < ptB) return false;

    // should be never here..
    return false;
}

//------------------------------------------------------------------------
// functions needed mainly for HHJetsInterface
bool jetSort (const jet_idx_btag& jA, const jet_idx_btag& jB)
{
    return (jA.btag > jB.btag);
}

bool jetPairSort (const jet_pair_mass& jA, const jet_pair_mass& jB)
{
    return (jA.inv_mass > jB.inv_mass);
}

bool jetPtSort (const TLorentzVector& jA, const TLorentzVector& jB)
{
    return (jA.Pt() > jB.Pt());
}

leps GetLeps(
    fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass, iRVec muoncharge,
    fRVec electronpt, fRVec electroneta, fRVec electronphi, fRVec electronmass, iRVec electroncharge,
    fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass, iRVec taucharge, iRVec taudecaymode,
    fRVec tadeeptauvse, fRVec taudeeptauvsmu, fRVec taudeeptauvsjet,
    int dau1_index, int dau2_index, int pairType)
{
  leps leptonsCollection;
    if (pairType == 0) { // MuTau
        leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
        leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
        leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
        leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
        leptonsCollection.dau1_charge = muoncharge.at(dau1_index);

        leptonsCollection.dau2_pt     = taupt.at(dau2_index);
        leptonsCollection.dau2_eta    = taueta.at(dau2_index);
        leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
        leptonsCollection.dau2_mass   = taumass.at(dau2_index);
        leptonsCollection.dau2_charge = taucharge.at(dau2_index);
        leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
        leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
        leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
        leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
    } 

    else if (pairType == 1) { // ETau
        leptonsCollection.dau1_pt     = electronpt.at(dau1_index);
        leptonsCollection.dau1_eta    = electroneta.at(dau1_index);
        leptonsCollection.dau1_phi    = electronphi.at(dau1_index);
        leptonsCollection.dau1_mass   = electronmass.at(dau1_index);
        leptonsCollection.dau1_charge = electroncharge.at(dau1_index);

        leptonsCollection.dau2_pt     = taupt.at(dau2_index);
        leptonsCollection.dau2_eta    = taueta.at(dau2_index);
        leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
        leptonsCollection.dau2_mass   = taumass.at(dau2_index);
        leptonsCollection.dau2_charge = taucharge.at(dau2_index);
        leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
        leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
        leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
        leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
    } 

    else if (pairType == 2) { // TauTau
        leptonsCollection.dau1_pt     = taupt.at(dau1_index);
        leptonsCollection.dau1_eta    = taueta.at(dau1_index);
        leptonsCollection.dau1_phi    = tauphi.at(dau1_index);
        leptonsCollection.dau1_mass   = taumass.at(dau1_index);
        leptonsCollection.dau1_charge = taucharge.at(dau1_index);
        leptonsCollection.dau1_DM     = taudecaymode.at(dau1_index);
        leptonsCollection.dau1_tauIdVSe   = tadeeptauvse.at(dau1_index);
        leptonsCollection.dau1_tauIdVSmu  = taudeeptauvsmu.at(dau1_index);
        leptonsCollection.dau1_tauIdVSjet = taudeeptauvsjet.at(dau1_index);

        leptonsCollection.dau2_pt     = taupt.at(dau2_index);
        leptonsCollection.dau2_eta    = taueta.at(dau2_index);
        leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
        leptonsCollection.dau2_mass   = taumass.at(dau2_index);
        leptonsCollection.dau2_charge = taucharge.at(dau2_index);
        leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
        leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
        leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
        leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
    } 

    else if (pairType == 3) { // MuMu
        leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
        leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
        leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
        leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
        leptonsCollection.dau1_charge = muoncharge.at(dau1_index);

        leptonsCollection.dau2_pt     = muonpt.at(dau2_index);
        leptonsCollection.dau2_eta    = muoneta.at(dau2_index);
        leptonsCollection.dau2_phi    = muonphi.at(dau2_index);
        leptonsCollection.dau2_mass   = muonmass.at(dau2_index);
        leptonsCollection.dau2_charge = muoncharge.at(dau2_index);
    } 

    else if (pairType == 4) { // EE
        leptonsCollection.dau1_pt     = electronpt.at(dau1_index);
        leptonsCollection.dau1_eta    = electroneta.at(dau1_index);
        leptonsCollection.dau1_phi    = electronphi.at(dau1_index);
        leptonsCollection.dau1_mass   = electronmass.at(dau1_index);
        leptonsCollection.dau1_charge = electroncharge.at(dau1_index);

        leptonsCollection.dau2_pt     = electronpt.at(dau2_index);
        leptonsCollection.dau2_eta    = electroneta.at(dau2_index);
        leptonsCollection.dau2_phi    = electronphi.at(dau2_index);
        leptonsCollection.dau2_mass   = electronmass.at(dau2_index);
        leptonsCollection.dau2_charge = electroncharge.at(dau2_index);
    } 

    else if (pairType == 5) { // EMu
        leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
        leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
        leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
        leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
        leptonsCollection.dau1_charge = muoncharge.at(dau1_index);

        leptonsCollection.dau2_pt     = electronpt.at(dau2_index);
        leptonsCollection.dau2_eta    = electroneta.at(dau2_index);
        leptonsCollection.dau2_phi    = electronphi.at(dau2_index);
        leptonsCollection.dau2_mass   = electronmass.at(dau2_index);
        leptonsCollection.dau2_charge = electroncharge.at(dau2_index);
    }

    return leptonsCollection;
}

leps_miscellanea GetLepsMisc(
    fRVec Muon_dxy, fRVec Muon_dxyErr, fRVec Muon_dz, fRVec Muon_dzErr, fRVec Muon_mediumId, fRVec Muon_pfRelIso04_all, fRVec Muon_tightId,
    fRVec Electron_dxy, fRVec Electron_dxyErr, fRVec Electron_dz, fRVec Electron_dzErr, fRVec Electron_mvaIso, fRVec Electron_mvaIso_WP80, fRVec Electron_mvaIso_WP90,
    fRVec Electron_mvaNoIso, fRVec Electron_mvaNoIso_WP80, fRVec Electron_mvaNoIso_WP90,
    fRVec Tau_dxy, fRVec Tau_dz, fRVec Tau_eleIdx, fRVec Tau_genPartFlav, fRVec Tau_genPartIdx, fRVec Tau_idAntiMu, fRVec Tau_idDecayModeNewDMs,
    fRVec Tau_muIdx, fRVec Tau_nSVs, fRVec Tau_rawDeepTau2018v2p5VSe, fRVec Tau_rawDeepTau2018v2p5VSjet, fRVec Tau_rawDeepTau2018v2p5VSmu,
    int dau1_index, int dau2_index, int pairType)
{
    leps_miscellanea miscellaneaCollection;
    if (pairType == 0) { // MuTau
        miscellaneaCollection.dau1_dxy            = Muon_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dxyErr         = Muon_dxyErr.at(dau1_index);
        miscellaneaCollection.dau1_dz             = Muon_dz.at(dau1_index);
        miscellaneaCollection.dau1_dzErr          = Muon_dzErr.at(dau1_index);
        miscellaneaCollection.dau1_mediumId       = Muon_mediumId.at(dau1_index);
        miscellaneaCollection.dau1_tightId        = Muon_tightId.at(dau1_index);
        miscellaneaCollection.dau1_pfRelIso04_all = Muon_pfRelIso04_all.at(dau1_index);

        miscellaneaCollection.dau2_dxy                     = Tau_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dz                      = Tau_dz.at(dau2_index);
        miscellaneaCollection.dau2_eleIdx                  = Tau_eleIdx.at(dau2_index);
        miscellaneaCollection.dau2_muIdx                   = Tau_muIdx.at(dau2_index);
        miscellaneaCollection.dau2_genPartFlav             = Tau_genPartFlav.at(dau2_index);
        miscellaneaCollection.dau2_genPartIdx              = Tau_genPartIdx.at(dau2_index);
        miscellaneaCollection.dau2_idAntiMu                = Tau_idAntiMu.at(dau2_index);
        miscellaneaCollection.dau2_idDecayModeNewDMs       = Tau_idDecayModeNewDMs.at(dau2_index);
        miscellaneaCollection.dau2_nSVs                    = Tau_nSVs.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSe   = Tau_rawDeepTau2018v2p5VSe.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSjet = Tau_rawDeepTau2018v2p5VSjet.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSmu  = Tau_rawDeepTau2018v2p5VSmu.at(dau2_index);
    }
    else if (pairType == 1) { // ETau
        miscellaneaCollection.dau1_dxy           = Electron_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dxyErr        = Electron_dxyErr.at(dau1_index);
        miscellaneaCollection.dau1_dz            = Electron_dz.at(dau1_index);
        miscellaneaCollection.dau1_dzErr         = Electron_dzErr.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso        = Electron_mvaIso.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso_WP80   = Electron_mvaIso_WP80.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso_WP90   = Electron_mvaIso_WP90.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso      = Electron_mvaNoIso.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso_WP80 = Electron_mvaNoIso_WP80.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso_WP90 = Electron_mvaNoIso_WP90.at(dau1_index);

        miscellaneaCollection.dau2_dxy                     = Tau_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dz                      = Tau_dz.at(dau2_index);
        miscellaneaCollection.dau2_eleIdx                  = Tau_eleIdx.at(dau2_index);
        miscellaneaCollection.dau2_muIdx                   = Tau_muIdx.at(dau2_index);
        miscellaneaCollection.dau2_genPartFlav             = Tau_genPartFlav.at(dau2_index);
        miscellaneaCollection.dau2_genPartIdx              = Tau_genPartIdx.at(dau2_index);
        miscellaneaCollection.dau2_idAntiMu                = Tau_idAntiMu.at(dau2_index);
        miscellaneaCollection.dau2_idDecayModeNewDMs       = Tau_idDecayModeNewDMs.at(dau2_index);
        miscellaneaCollection.dau2_nSVs                    = Tau_nSVs.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSe   = Tau_rawDeepTau2018v2p5VSe.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSjet = Tau_rawDeepTau2018v2p5VSjet.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSmu  = Tau_rawDeepTau2018v2p5VSmu.at(dau2_index);
    }
    else if (pairType == 2) { // TauTau
        miscellaneaCollection.dau1_dxy                     = Tau_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dz                      = Tau_dz.at(dau1_index);
        miscellaneaCollection.dau1_eleIdx                  = Tau_eleIdx.at(dau1_index);
        miscellaneaCollection.dau1_muIdx                   = Tau_muIdx.at(dau1_index);
        miscellaneaCollection.dau1_genPartFlav             = Tau_genPartFlav.at(dau1_index);
        miscellaneaCollection.dau1_genPartIdx              = Tau_genPartIdx.at(dau1_index);
        miscellaneaCollection.dau1_idAntiMu                = Tau_idAntiMu.at(dau1_index);
        miscellaneaCollection.dau1_idDecayModeNewDMs       = Tau_idDecayModeNewDMs.at(dau1_index);
        miscellaneaCollection.dau1_nSVs                    = Tau_nSVs.at(dau1_index);
        miscellaneaCollection.dau1_rawDeepTau2018v2p5VSe   = Tau_rawDeepTau2018v2p5VSe.at(dau1_index);
        miscellaneaCollection.dau1_rawDeepTau2018v2p5VSjet = Tau_rawDeepTau2018v2p5VSjet.at(dau1_index);
        miscellaneaCollection.dau1_rawDeepTau2018v2p5VSmu  = Tau_rawDeepTau2018v2p5VSmu.at(dau1_index);

        miscellaneaCollection.dau2_dxy                      = Tau_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dz                       = Tau_dz.at(dau2_index);
        miscellaneaCollection.dau2_eleIdx                   = Tau_eleIdx.at(dau2_index);
        miscellaneaCollection.dau2_muIdx                    = Tau_muIdx.at(dau2_index);
        miscellaneaCollection.dau2_genPartFlav              = Tau_genPartFlav.at(dau2_index);
        miscellaneaCollection.dau2_genPartIdx               = Tau_genPartIdx.at(dau2_index);
        miscellaneaCollection.dau2_idAntiMu                 = Tau_idAntiMu.at(dau2_index);
        miscellaneaCollection.dau2_idDecayModeNewDMs        = Tau_idDecayModeNewDMs.at(dau2_index);
        miscellaneaCollection.dau2_nSVs                     = Tau_nSVs.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSe    = Tau_rawDeepTau2018v2p5VSe.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSjet  = Tau_rawDeepTau2018v2p5VSjet.at(dau2_index);
        miscellaneaCollection.dau2_rawDeepTau2018v2p5VSmu   = Tau_rawDeepTau2018v2p5VSmu.at(dau2_index);
    }
    else if (pairType == 3) { // MuMu
        miscellaneaCollection.dau1_dxy            = Muon_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dxyErr         = Muon_dxyErr.at(dau1_index);
        miscellaneaCollection.dau1_dz             = Muon_dz.at(dau1_index);
        miscellaneaCollection.dau1_dzErr          = Muon_dzErr.at(dau1_index);
        miscellaneaCollection.dau1_mediumId       = Muon_mediumId.at(dau1_index);
        miscellaneaCollection.dau1_tightId        = Muon_tightId.at(dau1_index);
        miscellaneaCollection.dau1_pfRelIso04_all = Muon_pfRelIso04_all.at(dau1_index);

        miscellaneaCollection.dau2_dxy            = Muon_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dxyErr         = Muon_dxyErr.at(dau2_index);
        miscellaneaCollection.dau2_dz             = Muon_dz.at(dau2_index);
        miscellaneaCollection.dau2_dzErr          = Muon_dzErr.at(dau2_index);
        miscellaneaCollection.dau2_mediumId       = Muon_mediumId.at(dau2_index);
        miscellaneaCollection.dau2_tightId        = Muon_tightId.at(dau2_index);
        miscellaneaCollection.dau2_pfRelIso04_all = Muon_pfRelIso04_all.at(dau2_index);
    }
    else if (pairType == 4) { // EE
        miscellaneaCollection.dau1_dxy           = Electron_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dxyErr        = Electron_dxyErr.at(dau1_index);
        miscellaneaCollection.dau1_dz            = Electron_dz.at(dau1_index);
        miscellaneaCollection.dau1_dzErr         = Electron_dzErr.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso        = Electron_mvaIso.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso_WP80   = Electron_mvaIso_WP80.at(dau1_index);
        miscellaneaCollection.dau1_mvaIso_WP90   = Electron_mvaIso_WP90.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso      = Electron_mvaNoIso.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso_WP80 = Electron_mvaNoIso_WP80.at(dau1_index);
        miscellaneaCollection.dau1_mvaNoIso_WP90 = Electron_mvaNoIso_WP90.at(dau1_index);

        miscellaneaCollection.dau2_dxy           = Electron_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dxyErr        = Electron_dxyErr.at(dau2_index);
        miscellaneaCollection.dau2_dz            = Electron_dz.at(dau2_index);
        miscellaneaCollection.dau2_dzErr         = Electron_dzErr.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso        = Electron_mvaIso.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso_WP80   = Electron_mvaIso_WP80.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso_WP90   = Electron_mvaIso_WP90.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso      = Electron_mvaNoIso.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso_WP80 = Electron_mvaNoIso_WP80.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso_WP90 = Electron_mvaNoIso_WP90.at(dau2_index);
    }
    else if (pairType == 5) { // EMu
        miscellaneaCollection.dau1_dxy            = Muon_dxy.at(dau1_index);
        miscellaneaCollection.dau1_dxyErr         = Muon_dxyErr.at(dau1_index);
        miscellaneaCollection.dau1_dz             = Muon_dz.at(dau1_index);
        miscellaneaCollection.dau1_dzErr          = Muon_dzErr.at(dau1_index);
        miscellaneaCollection.dau1_mediumId       = Muon_mediumId.at(dau1_index);
        miscellaneaCollection.dau1_tightId        = Muon_tightId.at(dau1_index);
        miscellaneaCollection.dau1_pfRelIso04_all = Muon_pfRelIso04_all.at(dau1_index);

        miscellaneaCollection.dau2_dxy           = Electron_dxy.at(dau2_index);
        miscellaneaCollection.dau2_dxyErr        = Electron_dxyErr.at(dau2_index);
        miscellaneaCollection.dau2_dz            = Electron_dz.at(dau2_index);
        miscellaneaCollection.dau2_dzErr         = Electron_dzErr.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso        = Electron_mvaIso.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso_WP80   = Electron_mvaIso_WP80.at(dau2_index);
        miscellaneaCollection.dau2_mvaIso_WP90   = Electron_mvaIso_WP90.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso      = Electron_mvaNoIso.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso_WP80 = Electron_mvaNoIso_WP80.at(dau2_index);
        miscellaneaCollection.dau2_mvaNoIso_WP90 = Electron_mvaNoIso_WP90.at(dau2_index);
    }

    return miscellaneaCollection;
}

std::pair<TLorentzVector, TLorentzVector> GetLepsTLV(
    int index1, int index2,
    fRVec lep1_pt, fRVec lep1_eta, fRVec lep1_phi, fRVec lep1_mass,
    fRVec lep2_pt, fRVec lep2_eta, fRVec lep2_phi, fRVec lep2_mass)
{
    auto lep1_tlv = TLorentzVector(), lep2_tlv = TLorentzVector();
    lep1_tlv.SetPtEtaPhiM(lep1_pt[index1], lep1_eta[index1], lep1_phi[index1], lep1_mass[index1]);
    lep2_tlv.SetPtEtaPhiM(lep2_pt[index2], lep2_eta[index2], lep2_phi[index2], lep2_mass[index2]);
    return std::make_pair<TLorentzVector, TLorentzVector>((TLorentzVector) lep1_tlv, (TLorentzVector) lep2_tlv);
}

jets GetJets (
    bool hasresolvedak4, int bjet1_jetidx, int bjet2_jetidx,
    bool hasvbfak4, int vbfjet1_jetidx, int vbfjet2_jetidx,
    bool hasboostedak8, int fatjet_jetidx,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass, fRVec jet_btag, fRVec jet_hhbtag,
    fRVec jet_btagCvB, fRVec jet_btagCvL, fRVec jet_resolution, ui64RVec jet_smearseed,
    fRVec fatjet_pt, fRVec fatjet_eta, fRVec fatjet_phi, fRVec fatjet_mass, fRVec fatjet_btag)
{
    jets jetsCollection;
    if (hasresolvedak4) {
        jetsCollection.bjet1_pt         = jet_pt.at(bjet1_jetidx);
        jetsCollection.bjet1_eta        = jet_eta.at(bjet1_jetidx);
        jetsCollection.bjet1_phi        = jet_phi.at(bjet1_jetidx);
        jetsCollection.bjet1_mass       = jet_mass.at(bjet1_jetidx);
        jetsCollection.bjet1_btag       = jet_btag.at(bjet1_jetidx);
        jetsCollection.bjet1_hhbtag     = jet_hhbtag.at(bjet1_jetidx);
        jetsCollection.bjet1_btagCvB    = jet_btagCvB.at(bjet1_jetidx);
        jetsCollection.bjet1_btagCvL    = jet_btagCvL.at(bjet1_jetidx);
        jetsCollection.bjet1_resolution = jet_resolution.at(bjet1_jetidx);
        jetsCollection.bjet1_smearseed  = jet_smearseed.at(bjet1_jetidx);

        jetsCollection.bjet2_pt         = jet_pt.at(bjet2_jetidx);
        jetsCollection.bjet2_eta        = jet_eta.at(bjet2_jetidx);
        jetsCollection.bjet2_phi        = jet_phi.at(bjet2_jetidx);
        jetsCollection.bjet2_mass       = jet_mass.at(bjet2_jetidx);
        jetsCollection.bjet2_btag       = jet_btag.at(bjet2_jetidx);
        jetsCollection.bjet2_hhbtag     = jet_hhbtag.at(bjet2_jetidx);
        jetsCollection.bjet2_btagCvB    = jet_btagCvB.at(bjet2_jetidx);
        jetsCollection.bjet2_btagCvL    = jet_btagCvL.at(bjet2_jetidx);
        jetsCollection.bjet2_resolution = jet_resolution.at(bjet2_jetidx);
        jetsCollection.bjet2_smearseed  = jet_smearseed.at(bjet2_jetidx);
    }

    if (hasboostedak8) {
        jetsCollection.fatbjet_pt   = fatjet_pt.at(fatjet_jetidx);
        jetsCollection.fatbjet_eta  = fatjet_eta.at(fatjet_jetidx);
        jetsCollection.fatbjet_phi  = fatjet_phi.at(fatjet_jetidx);
        jetsCollection.fatbjet_mass = fatjet_mass.at(fatjet_jetidx);
        jetsCollection.fatbjet_btag = fatjet_btag.at(fatjet_jetidx);
    }

    if (hasvbfak4) {
        jetsCollection.vbfjet1_pt      = jet_pt.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_eta     = jet_eta.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_phi     = jet_phi.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_mass    = jet_mass.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_hhbtag  = jet_hhbtag.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_btagCvB = jet_btagCvB.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_btagCvL = jet_btagCvL.at(vbfjet1_jetidx);

        jetsCollection.vbfjet2_pt      = jet_pt.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_eta     = jet_eta.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_phi     = jet_phi.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_mass    = jet_mass.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_hhbtag  = jet_hhbtag.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_btagCvB = jet_btagCvB.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_btagCvL = jet_btagCvL.at(vbfjet2_jetidx);
    }

    return jetsCollection;
}

jets_miscellanea GetJetsMisc(
    bool hasresolvedak4, int bjet1_jetidx, int bjet2_jetidx,
    bool hasvbfak4, int vbfjet1_jetidx, int vbfjet2_jetidx,
    bool hasboostedak8, int fatjet_jetidx,
    fRVec Jet_PNetRegPtRawCorr, fRVec Jet_PNetRegPtRawCorrNeutrino, fRVec Jet_PNetRegPtRawRes, 
    fRVec Jet_btagPNetB, fRVec Jet_btagPNetCvB, fRVec Jet_btagPNetCvL, fRVec Jet_btagPNetQvG, 
    fRVec Jet_btagPNetTauVJet, fRVec Jet_btagRobustParTAK4B, fRVec Jet_btagRobustParTAK4CvB, 
    fRVec Jet_btagRobustParTAK4CvL, fRVec Jet_btagRobustParTAK4QG, fRVec Jet_genJetIdx, 
    fRVec Jet_rawFactor, fRVec FatJet_genJetAK8Idx, fRVec FatJet_msoftdrop,
    fRVec FatJet_particleNetWithMass_HbbvsQCD, fRVec FatJet_particleNet_XbbVsQCD,
    fRVec FatJet_particleNet_XteVsQCD, fRVec FatJet_particleNet_XtmVsQCD,
    fRVec FatJet_particleNet_XttVsQCD, fRVec FatJet_particleNet_massCorr, fRVec FatJet_rawFactor)
{
    jets_miscellanea miscellaneaCollection;
    if (hasresolvedak4) {
        miscellaneaCollection.bjet1_PNetRegPtRawCorr         = Jet_PNetRegPtRawCorr.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_PNetRegPtRawCorrNeutrino = Jet_PNetRegPtRawCorrNeutrino.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_PNetRegPtRawRes          = Jet_PNetRegPtRawRes.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagPNetB                = Jet_btagPNetB.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagPNetCvB              = Jet_btagPNetCvB.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagPNetCvL              = Jet_btagPNetCvL.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagPNetQvG              = Jet_btagPNetQvG.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagPNetTauVJet          = Jet_btagPNetTauVJet.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagRobustParTAK4B       = Jet_btagRobustParTAK4B.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagRobustParTAK4CvB     = Jet_btagRobustParTAK4CvB.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagRobustParTAK4CvL     = Jet_btagRobustParTAK4CvL.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_btagRobustParTAK4QG      = Jet_btagRobustParTAK4QG.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_genJetIdx                = Jet_genJetIdx.at(bjet1_jetidx);
        miscellaneaCollection.bjet1_rawFactor                = Jet_rawFactor.at(bjet1_jetidx);
        
        miscellaneaCollection.bjet2_PNetRegPtRawCorr         = Jet_PNetRegPtRawCorr.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_PNetRegPtRawCorrNeutrino = Jet_PNetRegPtRawCorrNeutrino.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_PNetRegPtRawRes          = Jet_PNetRegPtRawRes.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagPNetB                = Jet_btagPNetB.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagPNetCvB              = Jet_btagPNetCvB.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagPNetCvL              = Jet_btagPNetCvL.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagPNetQvG              = Jet_btagPNetQvG.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagPNetTauVJet          = Jet_btagPNetTauVJet.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagRobustParTAK4B       = Jet_btagRobustParTAK4B.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagRobustParTAK4CvB     = Jet_btagRobustParTAK4CvB.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagRobustParTAK4CvL     = Jet_btagRobustParTAK4CvL.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_btagRobustParTAK4QG      = Jet_btagRobustParTAK4QG.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_genJetIdx                = Jet_genJetIdx.at(bjet2_jetidx);
        miscellaneaCollection.bjet2_rawFactor                = Jet_rawFactor.at(bjet2_jetidx);
    }

    if (hasboostedak8) {
        miscellaneaCollection.fatjet_genJetAK8Idx                 = FatJet_genJetAK8Idx.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_msoftdrop                    = FatJet_msoftdrop.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNetWithMass_HbbvsQCD = FatJet_particleNetWithMass_HbbvsQCD.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNet_XbbVsQCD         = FatJet_particleNet_XbbVsQCD.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNet_XteVsQCD         = FatJet_particleNet_XteVsQCD.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNet_XtmVsQCD         = FatJet_particleNet_XtmVsQCD.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNet_XttVsQCD         = FatJet_particleNet_XttVsQCD.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_particleNet_massCorr         = FatJet_particleNet_massCorr.at(fatjet_jetidx);
        miscellaneaCollection.fatjet_rawFactor                    = FatJet_rawFactor.at(fatjet_jetidx);
    }

    if (hasvbfak4) {
        miscellaneaCollection.vbfjet1_PNetRegPtRawCorr         = Jet_PNetRegPtRawCorr.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_PNetRegPtRawCorrNeutrino = Jet_PNetRegPtRawCorrNeutrino.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_PNetRegPtRawRes          = Jet_PNetRegPtRawRes.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagPNetB                = Jet_btagPNetB.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagPNetCvB              = Jet_btagPNetCvB.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagPNetCvL              = Jet_btagPNetCvL.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagPNetQvG              = Jet_btagPNetQvG.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagPNetTauVJet          = Jet_btagPNetTauVJet.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagRobustParTAK4B       = Jet_btagRobustParTAK4B.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagRobustParTAK4CvB     = Jet_btagRobustParTAK4CvB.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagRobustParTAK4CvL     = Jet_btagRobustParTAK4CvL.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_btagRobustParTAK4QG      = Jet_btagRobustParTAK4QG.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_genJetIdx                = Jet_genJetIdx.at(vbfjet1_jetidx);
        miscellaneaCollection.vbfjet1_rawFactor                = Jet_rawFactor.at(vbfjet1_jetidx);

        miscellaneaCollection.vbfjet2_PNetRegPtRawCorr         = Jet_PNetRegPtRawCorr.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_PNetRegPtRawCorrNeutrino = Jet_PNetRegPtRawCorrNeutrino.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_PNetRegPtRawRes          = Jet_PNetRegPtRawRes.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagPNetB                = Jet_btagPNetB.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagPNetCvB              = Jet_btagPNetCvB.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagPNetCvL              = Jet_btagPNetCvL.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagPNetQvG              = Jet_btagPNetQvG.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagPNetTauVJet          = Jet_btagPNetTauVJet.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagRobustParTAK4B       = Jet_btagRobustParTAK4B.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagRobustParTAK4CvB     = Jet_btagRobustParTAK4CvB.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagRobustParTAK4CvL     = Jet_btagRobustParTAK4CvL.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_btagRobustParTAK4QG      = Jet_btagRobustParTAK4QG.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_genJetIdx                = Jet_genJetIdx.at(vbfjet2_jetidx);
        miscellaneaCollection.vbfjet2_rawFactor                = Jet_rawFactor.at(vbfjet2_jetidx);
    }

    return miscellaneaCollection;
}

std::pair<TLorentzVector, TLorentzVector> GetJetsTLV(
    int index1, int index2,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass)
{
    auto bjet1_tlv = TLorentzVector(), bjet2_tlv = TLorentzVector();
    if (index1 >= 0) {
        bjet1_tlv.SetPtEtaPhiM(Jet_pt[index1], Jet_eta[index1], Jet_phi[index1], Jet_mass[index1]);
    } 
    else {
        bjet1_tlv.SetPtEtaPhiM(-1, 0, 0, 0);
    }
    if (index2 >= 0) {
        bjet2_tlv.SetPtEtaPhiM(Jet_pt[index2], Jet_eta[index2], Jet_phi[index2], Jet_mass[index2]);
    } 
    else {
        bjet2_tlv.SetPtEtaPhiM(-1, 0, 0, 0);
    }
    return std::make_pair<TLorentzVector, TLorentzVector>((TLorentzVector) bjet1_tlv, (TLorentzVector) bjet2_tlv);
}
