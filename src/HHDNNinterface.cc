#include "Tools/Tools/interface/HHDNNinterface.h"

// Constructor
HHDNNinterface::HHDNNinterface (std::string model_dir, std::vector<std::string> requested,
    std::vector<float> target_kls, int year)
 : wrapper_(model_dir, 1, false),
   evt_proc_(false, requested, true)
{
  // Store target lambdas
  target_kls_ = target_kls;
  SetGlobalInputs(year);
}

// Destructor
HHDNNinterface::~HHDNNinterface() {}

// SetGlobalInputs: set inputs that never change with the events
void HHDNNinterface::SetGlobalInputs(int year)
{
  if (year == 2016)
    DNN_e_year_ = y16;
  else if (year == 2017)
    DNN_e_year_ = y17;
  else
    DNN_e_year_ = y18;
  DNN_spin_     = (Spin) 2;
  DNN_res_mass_ = 125.; // FIXME later
}


// SetEventInputs: set inputs that change every event
void HHDNNinterface::SetEventInputs(int channel, int is_boosted, int nvbf, unsigned long long int eventn,
  TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
  TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
  float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
  float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
  float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
  float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2)
{
  DNN_e_channel_    = (Channel) channel;
  DNN_is_boosted_   = is_boosted;
  DNN_n_vbf_        = nvbf;
  DNN_evt_          = eventn;

  // Taus
  DNN_l_1_.SetCoordinates(l1.Px(), l1.Py(), l1.Pz(), l1.M());
  if (DNN_e_channel_ == 0)
  { 
    DNN_l_1_.SetM(MU_MASS);
  }
  else if (DNN_e_channel_ == 1)
  {
    DNN_l_1_.SetM(E_MASS);
  }
  DNN_l_2_.SetCoordinates(l2.Px(), l2.Py(), l2.Pz(), l2.M());

  // b-jets
  DNN_b_1_.SetCoordinates(b1.Px(), b1.Py(), b1.Pz(), b1.M());
  DNN_b_2_.SetCoordinates(b2.Px(), b2.Py(), b2.Pz(), b2.M());

  // VBF jets
  DNN_vbf_1_.SetCoordinates(vbf1.Px(), vbf1.Py(), vbf1.Pz(), vbf1.M());
  DNN_vbf_2_.SetCoordinates(vbf2.Px(), vbf2.Py(), vbf2.Pz(), vbf2.M());

  // MET and SVfit
  DNN_met_  .SetCoordinates(met.Px(), met.Py(), 0, 0);
  DNN_svfit_.SetCoordinates(svfit.Px(), svfit.Py(), svfit.Pz(), svfit.M());

  // KinFit, MT2 and SVfit variables
  DNN_kinfit_mass_    = KinFitMass;
  DNN_kinfit_chi2_    = KinFitChi2;
  DNN_hh_kinfit_conv_ = KinFitConv;
  DNN_svfit_conv_     = SVfitConv;
  DNN_mt2_            = MT2;

  DNN_b_1_deepflav_ = deepFlav1;
  DNN_b_2_deepflav_ = deepFlav2;

  DNN_b_1_cvsl_     = CvsL_b1;
  DNN_b_2_cvsl_     = CvsL_b2;
  DNN_vbf_1_cvsl_   = CvsL_vbf1;
  DNN_vbf_2_cvsl_   = CvsL_vbf2;

  DNN_b_1_cvsb_     = CvsB_b1;
  DNN_b_2_cvsb_     = CvsB_b2;
  DNN_vbf_1_cvsb_   = CvsB_vbf1;
  DNN_vbf_2_cvsb_   = CvsB_vbf2;

  DNN_b_1_hhbtag_   = HHbtag_b1;
  DNN_b_2_hhbtag_   = HHbtag_b2;
  DNN_vbf_1_hhbtag_ = HHbtag_vbf1;
  DNN_vbf_2_hhbtag_ = HHbtag_vbf2;
}

// GetPredictions
std::vector<float> HHDNNinterface::GetPredictions()
{
  // Store results with predictions for each kl value
  std::vector<float> outDNN;

  for (unsigned int ikl = 0; ikl < target_kls_.size(); ikl++)
  {
    // Assign external values
    DNN_klambda_ = target_kls_.at(ikl);

    // Compute fatures
    std::vector<float> feat_vals = evt_proc_.process_as_vec(
        DNN_b_1_, DNN_b_2_, DNN_l_1_, DNN_l_2_, DNN_met_, DNN_svfit_, DNN_vbf_1_, DNN_vbf_2_,
        DNN_kinfit_mass_, DNN_kinfit_chi2_, DNN_mt2_, DNN_is_boosted_, DNN_b_1_deepflav_, DNN_b_2_deepflav_,
        DNN_e_channel_, DNN_e_year_, DNN_res_mass_, DNN_spin_, DNN_klambda_,
        DNN_n_vbf_, DNN_svfit_conv_, DNN_hh_kinfit_conv_,
        DNN_b_1_hhbtag_, DNN_b_2_hhbtag_, DNN_vbf_1_hhbtag_, DNN_vbf_2_hhbtag_,
        DNN_b_1_cvsl_, DNN_b_2_cvsl_, DNN_vbf_1_cvsl_, DNN_vbf_2_cvsl_,
        DNN_b_1_cvsb_, DNN_b_2_cvsb_, DNN_vbf_1_cvsb_, DNN_vbf_2_cvsb_,
        0, 0, 0, // cv, c2v, c3
        true);

    std::vector<std::string> feats_names = evt_proc_.get_feats();

    // std::cout << " Feats size : " << feat_vals.size() << std::endl;
    // for (uint j=0; j<feat_vals.size(); j++)
    // {
    //  std::cout << " - " << feats_names.at(j) << " : " << feat_vals.at(j) << std::endl;
    // }

    // Get model prediction
    float DNN_pred = wrapper_.predict(feat_vals, DNN_evt_);

    // Store prediction
    outDNN.push_back(DNN_pred);
  }

  return outDNN;
}

// GetDefaultInputsForTraining
std::vector<float> HHDNNinterface::GetDeafultInputs(
  int channel, int is_boosted, int nvbf, unsigned long long int eventn,
  TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
  TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
  float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
  float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
  float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
  float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2)
{

  SetEventInputs(channel, is_boosted, nvbf, eventn,
  b1, b2, l1, l2, 
  vbf1, vbf2, met, svfit, 
  KinFitMass, KinFitChi2, KinFitConv, SVfitConv, MT2,
  deepFlav1, deepFlav2, CvsL_b1, CvsL_b2, CvsL_vbf1, CvsL_vbf2,
  CvsB_b1, CvsB_b2, CvsB_vbf1, CvsB_vbf2,
  HHbtag_b1, HHbtag_b2, HHbtag_vbf1, HHbtag_vbf2);
  
  // Assign external values
  DNN_klambda_ = target_kls_.at(0);

  // Compute fatures
  std::vector<float> feat_vals = evt_proc_.process_as_vec(
      DNN_b_1_, DNN_b_2_, DNN_l_1_, DNN_l_2_, DNN_met_, DNN_svfit_, DNN_vbf_1_, DNN_vbf_2_,
      DNN_kinfit_mass_, DNN_kinfit_chi2_, DNN_mt2_, DNN_is_boosted_, DNN_b_1_deepflav_, DNN_b_2_deepflav_,
      DNN_e_channel_, DNN_e_year_, DNN_res_mass_, DNN_spin_, DNN_klambda_,
      DNN_n_vbf_, DNN_svfit_conv_, DNN_hh_kinfit_conv_,
      DNN_b_1_hhbtag_, DNN_b_2_hhbtag_, DNN_vbf_1_hhbtag_, DNN_vbf_2_hhbtag_,
      DNN_b_1_cvsl_, DNN_b_2_cvsl_, DNN_vbf_1_cvsl_, DNN_vbf_2_cvsl_,
      DNN_b_1_cvsb_, DNN_b_2_cvsb_, DNN_vbf_1_cvsb_, DNN_vbf_2_cvsb_,
      0, 0, 0, // cv, c2v, c3
      true);

  return feat_vals;
}

std::vector<float> HHDNNinterface::GetPredictionsWithInputs(
  int channel, int is_boosted, int nvbf, unsigned long long int eventn,
  TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
  TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
  float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
  float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
  float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
  float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2)
{
  SetEventInputs(channel, is_boosted, nvbf, eventn,
    b1, b2, l1, l2, 
    vbf1, vbf2, met, svfit, 
    KinFitMass, KinFitChi2, KinFitConv, SVfitConv, MT2,
    deepFlav1, deepFlav2, CvsL_b1, CvsL_b2, CvsL_vbf1, CvsL_vbf2,
    CvsB_b1, CvsB_b2, CvsB_vbf1, CvsB_vbf2,
    HHbtag_b1, HHbtag_b2, HHbtag_vbf1, HHbtag_vbf2);
  return GetPredictions();
}

// wrapper function to be used in the .py that works with non-flat pNTples
// and reads the vector branches using the indexes of teh selected objects
fRVec HHDNNinterface::get_dnn_outputs(int pairType, int hasBoostedAK8, int event,
    int dau1_index, int dau2_index, int bjet1_index, int bjet2_index,
    bool hasResolvedAK4, int vbfjet1_index, int vbfjet2_index,
    fRVec muon_pt, fRVec muon_eta, fRVec muon_phi, fRVec muon_mass,
    fRVec electron_pt, fRVec electron_eta, fRVec electron_phi, fRVec electron_mass,
    fRVec tau_pt, fRVec tau_eta, fRVec tau_phi, fRVec tau_mass,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass,
    float htt_sv_pt, float htt_sv_eta, float htt_sv_phi, float htt_sv_mass,
    float HHKinFit_mass, float HHKinFit_chi2, float met_pt, float met_phi,
    fRVec Jet_btagDeepFlavB, fRVec Jet_btagDeepFlavCvL, fRVec Jet_btagDeepFlavCvB,
    fRVec Jet_HHbtag
)
{
    if (!hasResolvedAK4 || pairType < 0 || pairType > 2) {
        std::vector<float> dummy_result(1,-999.);
        return dummy_result;
    }

    float dau1_pt, dau1_eta, dau1_phi, dau1_mass, dau2_pt, dau2_eta, dau2_phi, dau2_mass;

    // pairType | Our Def. | DNN
    // ------------------------
    // mutau   |    0     |  1
    // etau    |    1     |  2
    // tautau  |    2     |  0

    int channel;
    if(pairType == 0)
        channel = 1;
    else if (pairType == 1)
        channel = 2;
    else
        channel = 0;

    if (pairType == 0) {
        dau1_pt = muon_pt.at(dau1_index);
        dau1_eta = muon_eta.at(dau1_index);
        dau1_phi = muon_phi.at(dau1_index);
        dau1_mass = muon_mass.at(dau1_index);
    } else if (pairType == 1) {
        dau1_pt = electron_pt.at(dau1_index);
        dau1_eta = electron_eta.at(dau1_index);
        dau1_phi = electron_phi.at(dau1_index);
        dau1_mass = electron_mass.at(dau1_index);
    } else if (pairType == 2) {
        dau1_pt = tau_pt.at(dau1_index);
        dau1_eta = tau_eta.at(dau1_index);
        dau1_phi = tau_phi.at(dau1_index);
        dau1_mass = tau_mass.at(dau1_index);
    }
    dau2_pt = tau_pt.at(dau2_index);
    dau2_eta = tau_eta.at(dau2_index);
    dau2_phi = tau_phi.at(dau2_index);
    dau2_mass = tau_mass.at(dau2_index);

    auto dau1_tlv = TLorentzVector();
    auto dau2_tlv = TLorentzVector();
    auto bjet1_tlv = TLorentzVector();
    auto bjet2_tlv = TLorentzVector();
    auto vbfjet1_tlv = TLorentzVector();
    auto vbfjet2_tlv = TLorentzVector();

    dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
    dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
    bjet1_tlv.SetPtEtaPhiM(jet_pt.at(bjet1_index), jet_eta.at(bjet1_index),
        jet_phi.at(bjet1_index), jet_mass.at(bjet1_index));
    bjet2_tlv.SetPtEtaPhiM(jet_pt.at(bjet2_index), jet_eta.at(bjet2_index),
        jet_phi.at(bjet2_index), jet_mass.at(bjet2_index));

    int nvbf = 0;
    if (vbfjet1_index >= 0) {
        vbfjet1_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet1_index), jet_eta.at(vbfjet1_index),
            jet_phi.at(vbfjet1_index), jet_mass.at(vbfjet1_index));
        vbfjet2_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet2_index), jet_eta.at(vbfjet2_index),
            jet_phi.at(vbfjet2_index), jet_mass.at(vbfjet2_index));
        nvbf = 2;
    } else {
        vbfjet1_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
        vbfjet2_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
    }
    auto met_tlv = TLorentzVector();
    met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);

    auto htt_svfit_tlv = TLorentzVector();
    if (htt_sv_mass > 0)
        htt_svfit_tlv.SetPtEtaPhiM(htt_sv_pt, htt_sv_eta, htt_sv_phi, htt_sv_mass);
    else
        htt_svfit_tlv.SetPtEtaPhiM(1., 1., 1., 1.);

    // MT2 computation
    asymm_mt2_lester_bisect::disableCopyrightMessage();
    double MT2 = asymm_mt2_lester_bisect::get_mT2(
        bjet1_tlv.M(), bjet1_tlv.Px(), bjet1_tlv.Py(),
        bjet2_tlv.M(), bjet2_tlv.Px(), bjet2_tlv.Py(),
        dau1_tlv.Px() + dau2_tlv.Px() + met_tlv.Px(),
        dau1_tlv.Py() + dau2_tlv.Py() + met_tlv.Py(),
        dau1_tlv.M(), dau2_tlv.M(), 0.);

    float deepFlav1 = -1., deepFlav2 = -1., CvsL_b1 = -1., CvsL_b2 = -1.,
        CvsL_vbf1 = -1., CvsL_vbf2 = -1., CvsB_b1 = -1., CvsB_b2 = -1.,
        CvsB_vbf1 = -1., CvsB_vbf2 = -1., HHbtag_b1 = -1., HHbtag_b2 = -1.,
        HHbtag_vbf1 = -1., HHbtag_vbf2 = -1.;
    deepFlav1 = Jet_btagDeepFlavB.at(bjet1_index);
    deepFlav2 = Jet_btagDeepFlavB.at(bjet2_index);
    CvsL_b1 = Jet_btagDeepFlavCvL.at(bjet1_index);
    CvsL_b2 = Jet_btagDeepFlavCvL.at(bjet2_index);
    CvsB_b1 = Jet_btagDeepFlavCvB.at(bjet1_index);
    CvsB_b2 = Jet_btagDeepFlavCvB.at(bjet2_index);
    HHbtag_b1 = Jet_HHbtag.at(bjet1_index);
    HHbtag_b2 = Jet_HHbtag.at(bjet2_index);
    if (vbfjet1_index >= 0) {
        CvsL_vbf1 = Jet_btagDeepFlavCvL.at(vbfjet1_index);
        CvsL_vbf2 = Jet_btagDeepFlavCvL.at(vbfjet2_index);
        CvsB_vbf1 = Jet_btagDeepFlavCvB.at(vbfjet1_index);
        CvsB_vbf2 = Jet_btagDeepFlavCvB.at(vbfjet2_index);
        HHbtag_vbf1 = Jet_HHbtag.at(vbfjet1_index);
        HHbtag_vbf2 = Jet_HHbtag.at(vbfjet2_index);
    }

    return GetPredictionsWithInputs(
      channel, hasBoostedAK8, nvbf, event,
      bjet1_tlv, bjet2_tlv, dau1_tlv, dau2_tlv,
      vbfjet1_tlv, vbfjet2_tlv, met_tlv, htt_svfit_tlv,
      HHKinFit_mass, HHKinFit_chi2, (HHKinFit_chi2 >= 0), (htt_sv_mass >= 0), MT2,
      deepFlav1, deepFlav2, CvsL_b1, CvsL_b2, CvsL_vbf1, CvsL_vbf2,
      CvsB_b1, CvsB_b2, CvsB_vbf1, CvsB_vbf2,
      HHbtag_b1, HHbtag_b2, HHbtag_vbf1, HHbtag_vbf2);
}

// wrapper function to be used in the .py that works with flat pNTples
// and already gets the information of the selected objects
fRVec HHDNNinterface::get_dnn_outputs(int pairType, int event,
    bool hasResolvedAK4, bool hasBoostedAK8, bool hasVBFAK4,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float bjet1_pt, float bjet1_eta, float bjet1_phi, float bjet1_mass,
    float bjet2_pt, float bjet2_eta, float bjet2_phi, float bjet2_mass,
    float bjet1_btag, float bjet1_btagCvB, float bjet1_btagCvL, float bjet1_hhbtag,
    float bjet2_btag, float bjet2_btagCvB, float bjet2_btagCvL, float bjet2_hhbtag,
    float vbfjet1_pt, float vbfjet1_eta, float vbfjet1_phi, float vbfjet1_mass,
    float vbfjet2_pt, float vbfjet2_eta, float vbfjet2_phi, float vbfjet2_mass,
    float vbfjet1_btagCvB, float vbfjet1_btagCvL, float vbfjet1_hhbtag,
    float vbfjet2_btagCvB, float vbfjet2_btagCvL, float vbfjet2_hhbtag,
    float htt_sv_pt, float htt_sv_eta, float htt_sv_phi, float htt_sv_mass,
    float HHKinFit_mass, float HHKinFit_chi2, float met_pt, float met_phi
)
{
    if (!hasResolvedAK4 || pairType < 0 || pairType > 2) {
        std::vector<float> dummy_result(1,-999.);
        return dummy_result;
    }

    // pairType | Our Def. | DNN
    // ------------------------
    // mutau   |    0     |  1
    // etau    |    1     |  2
    // tautau  |    2     |  0

    int channel;
    if(pairType == 0)
        channel = 1;
    else if (pairType == 1)
        channel = 2;
    else
        channel = 0;

    auto dau1_tlv = TLorentzVector();
    auto dau2_tlv = TLorentzVector();
    auto bjet1_tlv = TLorentzVector();
    auto bjet2_tlv = TLorentzVector();
    auto vbfjet1_tlv = TLorentzVector();
    auto vbfjet2_tlv = TLorentzVector();

    dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
    dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
    bjet1_tlv.SetPtEtaPhiM(bjet1_pt, bjet1_eta, bjet1_phi, bjet1_mass);
    bjet2_tlv.SetPtEtaPhiM(bjet2_pt, bjet2_eta, bjet2_phi, bjet2_mass);

    int nvbf = 0;
    if (hasVBFAK4) {
      vbfjet1_tlv.SetPtEtaPhiM(vbfjet1_pt, vbfjet1_eta, vbfjet1_phi, vbfjet1_mass);
      vbfjet2_tlv.SetPtEtaPhiM(vbfjet2_pt, vbfjet2_eta, vbfjet2_phi, vbfjet2_mass);
      nvbf = 2;
    } else {
      vbfjet1_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
      vbfjet2_tlv.SetPtEtaPhiM(1., 1., 1., 1.);
    }

    auto met_tlv = TLorentzVector();
    met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);

    auto htt_svfit_tlv = TLorentzVector();
    if (htt_sv_mass > 0)
      htt_svfit_tlv.SetPtEtaPhiM(htt_sv_pt, htt_sv_eta, htt_sv_phi, htt_sv_mass);
    else
      htt_svfit_tlv.SetPtEtaPhiM(1., 1., 1., 1.);

    // MT2 computation
    asymm_mt2_lester_bisect::disableCopyrightMessage();
    double MT2 = asymm_mt2_lester_bisect::get_mT2(
        bjet1_tlv.M(), bjet1_tlv.Px(), bjet1_tlv.Py(),
        bjet2_tlv.M(), bjet2_tlv.Px(), bjet2_tlv.Py(),
        dau1_tlv.Px() + dau2_tlv.Px() + met_tlv.Px(),
        dau1_tlv.Py() + dau2_tlv.Py() + met_tlv.Py(),
        dau1_tlv.M(), dau2_tlv.M(), 0.);

    float CvsL_vbf1 = -1., CvsL_vbf2 = -1., CvsB_vbf1 = -1.,
          CvsB_vbf2 = -1., HHbtag_vbf1 = -1., HHbtag_vbf2 = -1.;
    if (hasVBFAK4) {
      CvsL_vbf1 = vbfjet1_btagCvL;
      CvsL_vbf2 = vbfjet2_btagCvL;
      CvsB_vbf1 = vbfjet1_btagCvB;
      CvsB_vbf2 = vbfjet2_btagCvB;
      HHbtag_vbf1 = vbfjet1_hhbtag;
      HHbtag_vbf2 = vbfjet2_hhbtag;
    }

    return GetPredictionsWithInputs(
      channel, hasBoostedAK8, nvbf, event,
      bjet1_tlv, bjet2_tlv, dau1_tlv, dau2_tlv,
      vbfjet1_tlv, vbfjet2_tlv, met_tlv, htt_svfit_tlv,
      HHKinFit_mass, HHKinFit_chi2, (HHKinFit_chi2 >= 0), (htt_sv_mass >= 0), MT2,
      bjet1_btag, bjet2_btag, bjet1_btagCvL, bjet2_btagCvL, CvsL_vbf1, CvsL_vbf2,
      bjet1_btagCvB, bjet2_btagCvB, CvsB_vbf1, CvsB_vbf2,
      bjet1_hhbtag, bjet2_hhbtag, HHbtag_vbf1, HHbtag_vbf2);
}
