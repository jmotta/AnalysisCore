#include "Tools/Tools/interface/HHJetsInterface.h"

// Constructor
HHJetsInterface::HHJetsInterface (std::string model_0, std::string model_1, int year, std::string runPeriod, bool extEtaAcc,
                                  bool bypass_HHBTag, float btag_algo_cut):
  HHbtagger_(std::array<std::string, 2> { {model_0, model_1} })
{
  year_ = year;
  runPeriod_ = runPeriod;
  if (extEtaAcc) max_bjet_eta = 2.5;
  bypass_HHBTag_ = bypass_HHBTag;
  btag_algo_cut_ = btag_algo_cut;
}


// Destructor
HHJetsInterface::~HHJetsInterface() {}

// definition compatible with Run-2 analyses using CHS jets and need Jet_puId
jets_output HHJetsInterface::GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float MET_pt, float MET_phi)
{
  std::vector<float> all_HHbtag_scores;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    all_HHbtag_scores.push_back(-999.);
  }

  int hasResolvedAK4_ = 0;
  int bjet1_idx = -1;
  int bjet2_idx = -1;
  int hasVBFAK4_ = 0;
  int vbfjet1_idx = -1;
  int vbfjet2_idx = -1;
  int hasBoostedAK8_ = 0;
  int fatjet_idx = -1;
  int fatjet2jet1_idx = -1;
  int fatjet2jet2_idx = -1;
  std::vector<int> ctjet_indexes, fwjet_indexes;

  if (pairType < 0) {
    return jets_output({all_HHbtag_scores, hasResolvedAK4_, bjet1_idx, bjet2_idx, 
      hasVBFAK4_, vbfjet1_idx, vbfjet2_idx, ctjet_indexes, fwjet_indexes,
      hasBoostedAK8_, fatjet_idx, fatjet2jet1_idx, fatjet2jet2_idx});
  }

  auto dau1_tlv = TLorentzVector();
  auto dau2_tlv = TLorentzVector();
  auto met_tlv = TLorentzVector();
  dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
  dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
  met_tlv.SetPxPyPzE(MET_pt * cos(MET_phi), MET_pt * sin(MET_phi), 0, MET_pt);

  std::vector <jet_idx_btag> jet_indexes;
  std::vector <int> all_jet_indexes;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if ((Jet_puId[ijet] < 1 && Jet_pt[ijet] <= 50) || Jet_jetId[ijet] != 6)
      continue;
    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    // do cleaning in 0.5 cone only for hadronic taus
    if ((pairType >=0 && pairType <= 2 && jet_tlv.DeltaR(dau2_tlv) < 0.5) ||
        (pairType == 2 && jet_tlv.DeltaR(dau1_tlv) < 0.5) )
      continue;
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_bjet_eta)
      jet_indexes.push_back(jet_idx_btag({(int) ijet, Jet_btag[ijet]}));
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_vbfjet_eta)
      all_jet_indexes.push_back(ijet);
  }

  // new definiton of boosted only requiring one AK8 jet (no subjets match)
  std::vector <jet_idx_btag> fatjet_indexes;
  for (size_t ifatjet = 0; ifatjet < FatJet_pt.size(); ifatjet++) {
    auto fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[ifatjet], FatJet_eta[ifatjet],
      FatJet_phi[ifatjet], FatJet_mass[ifatjet]);
    if (fatjet_tlv.Pt() < 250 || fabs(fatjet_tlv.Eta()) > max_bjet_eta) continue;
    if (FatJet_msoftdrop.at(ifatjet) < 30) continue;
    if (FatJet_jetId[ifatjet] != 6) continue;
    // do cleaning in 0.8 cone only for hadronic taus
    if ((pairType >=0 && pairType <= 2 && fatjet_tlv.DeltaR(dau2_tlv) < 0.8) ||
        (pairType == 2 && fatjet_tlv.DeltaR(dau1_tlv) < 0.8) )
      continue;
    fatjet_indexes.push_back(jet_idx_btag({(int) ifatjet, FatJet_particleNet_XbbVsQCD.at(ifatjet)}));
  }
  if (fatjet_indexes.size() != 0) {
    hasBoostedAK8_ = 1;
    std::stable_sort(fatjet_indexes.begin(), fatjet_indexes.end(), jetSort);
    fatjet_idx = fatjet_indexes[0].idx;

    TLorentzVector fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[fatjet_idx], FatJet_eta[fatjet_idx],
                            FatJet_phi[fatjet_idx], FatJet_mass[fatjet_idx]);

    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet_index = all_jet_indexes[ijet];
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet_index], Jet_eta[jet_index],
                           Jet_phi[jet_index], Jet_mass[jet_index]);
      
      if (jet_tlv.DeltaR(fatjet_tlv) < 0.8) {
        if (fatjet2jet1_idx == -1)
          fatjet2jet1_idx = jet_index;
        else if (fatjet2jet2_idx == -1)
          fatjet2jet2_idx = jet_index;
        else
          break; // jets are order in pt by construction
      }
    }
  }

  if (jet_indexes.size() >= 2) {
    hasResolvedAK4_ = 1;

    std::stable_sort(jet_indexes.begin(), jet_indexes.end(), jetSort);

    auto htt_tlv = dau1_tlv + dau2_tlv;
    std::vector <float> HHbtag_jet_pt_, HHbtag_jet_eta_, HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_;
    std::vector <float> HHbtag_jet_htt_deta_, HHbtag_jet_htt_dphi_, HHbtag_jet_btagOutput_;

    for (auto &jet : jet_indexes) {
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet.idx], Jet_eta[jet.idx], Jet_phi[jet.idx], Jet_mass[jet.idx]);
      HHbtag_jet_pt_.push_back(jet_tlv.Pt());
      HHbtag_jet_eta_.push_back(jet_tlv.Eta());
      HHbtag_rel_jet_M_pt_.push_back(jet_tlv.M() / jet_tlv.Pt());
      HHbtag_rel_jet_E_pt_.push_back(jet_tlv.E() / jet_tlv.Pt());
      HHbtag_jet_htt_deta_.push_back(htt_tlv.Eta() - jet_tlv.Eta());
      HHbtag_jet_htt_dphi_.push_back(ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, jet_tlv));
      HHbtag_jet_btagOutput_.push_back(jet.btag);
    }

    auto HHbtag_htt_met_dphi_ = (float) ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, met_tlv);
    auto HHbtag_htt_scalar_pt_ = (float) (dau1_tlv.Pt() + dau2_tlv.Pt());
    auto HHbtag_rel_met_pt_htt_pt_ = (float) met_tlv.Pt() / HHbtag_htt_scalar_pt_;
    auto HHbtag_htt_pt_ = (float) htt_tlv.Pt();
    auto HHbtag_htt_eta_ = (float) htt_tlv.Eta();
    int HHbtag_eraid_ = -1;

    if (year_ == 2022) {
      if (runPeriod_ == "preEE")
        HHbtag_eraid_ = 0;
      else if (runPeriod_ == "postEE")
        HHbtag_eraid_ = 1;
    }
    else if (year_ == 2023) {
      if (runPeriod_ == "preBPix")
        HHbtag_eraid_ = 2;
      else if (runPeriod_ == "postBPix")
        HHbtag_eraid_ = 3;
    }
    else
      throw std::invalid_argument("HHJetsInterface::HHBtag - Invalid year");

    auto HHbtag_scores = HHbtagger_.GetScore(HHbtag_jet_pt_, HHbtag_jet_eta_,
      HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_, HHbtag_jet_htt_deta_,
      HHbtag_jet_btagOutput_, HHbtag_jet_htt_dphi_, HHbtag_eraid_, pairType,
      HHbtag_htt_pt_, HHbtag_htt_eta_, HHbtag_htt_met_dphi_,
      HHbtag_rel_met_pt_htt_pt_, HHbtag_htt_scalar_pt_, event);

    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      all_HHbtag_scores[jet_indexes[ijet].idx] = HHbtag_scores[ijet];
    }
    std::vector <jet_idx_btag> jet_indexes_hhbtag;
    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      jet_indexes_hhbtag.push_back(jet_idx_btag({jet_indexes[ijet].idx, HHbtag_scores[ijet]}));
    }

    // Choose b-jets with HHBtag or with tagger
    if (!bypass_HHBTag_) {
      std::stable_sort(jet_indexes_hhbtag.begin(), jet_indexes_hhbtag.end(), jetSort);
      bjet1_idx = jet_indexes_hhbtag[0].idx;
      bjet2_idx = jet_indexes_hhbtag[1].idx;
    }
    else {
      bjet1_idx = jet_indexes[0].idx;
      bjet2_idx = jet_indexes[1].idx;
    }

    // Order 2 selected b-jets by pt
    if (Jet_pt[bjet1_idx] < Jet_pt[bjet2_idx]) {
        auto aux = bjet1_idx;
        bjet1_idx = bjet2_idx;
        bjet2_idx = aux;
    }
  }

  if (all_jet_indexes.size() >= 2) { // 0 fatjet2jet + 0 bjets + 2 vbf jets
    std::vector <jet_pair_mass> vbfjet_indexes;
    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet1_index = all_jet_indexes[ijet];
      auto jet1_tlv = TLorentzVector();
      jet1_tlv.SetPtEtaPhiM(Jet_pt[jet1_index], Jet_eta[jet1_index],
                            Jet_phi[jet1_index], Jet_mass[jet1_index]);
      
      if (hasBoostedAK8_ && (jet1_index == fatjet2jet1_idx || jet1_index == fatjet2jet2_idx))
        continue;
      if (hasResolvedAK4_ && (jet1_index == bjet1_idx || jet1_index == bjet2_idx))
        continue;
      
      for (size_t jjet = ijet + 1; jjet < all_jet_indexes.size(); jjet++) {
        auto jet2_index = all_jet_indexes[jjet];
        auto jet2_tlv = TLorentzVector();
        jet2_tlv.SetPtEtaPhiM(Jet_pt[jet2_index], Jet_eta[jet2_index],
                              Jet_phi[jet2_index], Jet_mass[jet2_index]);
        
        if (hasBoostedAK8_ && (jet2_index == fatjet2jet1_idx || jet2_index == fatjet2jet2_idx))
          continue;
        if (hasResolvedAK4_ && (jet2_index == bjet1_idx || jet2_index == bjet2_idx))
          continue;
        if (Jet_pt[jet1_index] < 20 || Jet_pt[jet2_index] < 20)
          continue; // cut was originally 30, keeping it at 20 for initial studies
        
        auto jj_tlv = jet1_tlv + jet2_tlv;
        vbfjet_indexes.push_back(jet_pair_mass({jet1_index, jet2_index, (float) jj_tlv.M()}));
      }
    }

    if (vbfjet_indexes.size() > 0) {
      std::stable_sort(vbfjet_indexes.begin(), vbfjet_indexes.end(), jetPairSort);
      vbfjet1_idx = vbfjet_indexes[0].idx1;
      vbfjet2_idx = vbfjet_indexes[0].idx2;
      hasVBFAK4_ = 1;

      if (Jet_pt[vbfjet1_idx] < Jet_pt[vbfjet2_idx]) {
        auto aux = vbfjet1_idx;
        vbfjet1_idx = vbfjet2_idx;
        vbfjet2_idx = aux;
      }
    }
  }

  // additional central and forward jets
  for (auto & ijet : all_jet_indexes) {
    if ((int) ijet == bjet1_idx || (int) ijet == bjet2_idx
          || (int) ijet == vbfjet1_idx || (int) ijet == vbfjet2_idx
          || (int) ijet == fatjet2jet1_idx || (int) ijet == fatjet2jet2_idx)
      continue;
    if (fabs(Jet_eta[ijet]) < max_bjet_eta)
      ctjet_indexes.push_back(ijet);
    else if (fabs(Jet_eta[ijet]) < max_vbfjet_eta && Jet_pt[ijet] > 30)
      fwjet_indexes.push_back(ijet);
  }

  return jets_output({all_HHbtag_scores, hasResolvedAK4_, bjet1_idx, bjet2_idx,
    hasVBFAK4_, vbfjet1_idx, vbfjet2_idx, ctjet_indexes, fwjet_indexes,
    hasBoostedAK8_, fatjet_idx, fatjet2jet1_idx, fatjet2jet2_idx});
}

// definition compatible with Run-3 analyses using PUPPI jets and don't need Jet_puId
// this definition uses the new fatjet without subjet apporoach
jets_output HHJetsInterface::GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float MET_pt, float MET_phi)
{
  std::vector<float> all_HHbtag_scores;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    all_HHbtag_scores.push_back(-999.);
  }

  int hasResolvedAK4_ = 0;
  int bjet1_idx = -1;
  int bjet2_idx = -1;
  int hasVBFAK4_ = 0;
  int vbfjet1_idx = -1;
  int vbfjet2_idx = -1;
  int hasBoostedAK8_ = 0;
  int fatjet_idx = -1;
  int fatjet2jet1_idx = -1;
  int fatjet2jet2_idx = -1;
  std::vector<int> ctjet_indexes, fwjet_indexes;

  if (pairType < 0) {
    return jets_output({all_HHbtag_scores, hasResolvedAK4_, bjet1_idx, bjet2_idx,
      hasVBFAK4_, vbfjet1_idx, vbfjet2_idx, ctjet_indexes, fwjet_indexes,
      hasBoostedAK8_, fatjet_idx, fatjet2jet1_idx, fatjet2jet2_idx});
  }

  auto dau1_tlv = TLorentzVector();
  auto dau2_tlv = TLorentzVector();
  auto met_tlv = TLorentzVector();
  dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
  dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
  met_tlv.SetPxPyPzE(MET_pt * cos(MET_phi), MET_pt * sin(MET_phi), 0, MET_pt);

  std::vector <jet_idx_btag> jet_indexes;
  std::vector <int> all_jet_indexes;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if (Jet_jetId[ijet] != 6)
      continue;
    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    // do cleaning in 0.5 cone only for hadronic taus
    if ((pairType >=0 && pairType <= 2 && jet_tlv.DeltaR(dau2_tlv) < 0.5) ||
        (pairType == 2 && jet_tlv.DeltaR(dau1_tlv) < 0.5) )
      continue;
    // preliminary horns removal recommandation: https://twiki.cern.ch/twiki/bin/view/CMS/JetMET?rev=293#Run3_recommendations
    if (Jet_pt[ijet] < 50 && fabs(Jet_eta[ijet]) > 2.5 && fabs(Jet_eta[ijet]) < 3.0)
      continue;
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_bjet_eta)
      jet_indexes.push_back(jet_idx_btag({(int) ijet, Jet_btag[ijet]}));
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_vbfjet_eta)
      all_jet_indexes.push_back(ijet);
  }

  // new definiton of boosted only requiring one AK8 jet (no subjets match)
  std::vector <jet_idx_btag> fatjet_indexes;
  for (size_t ifatjet = 0; ifatjet < FatJet_pt.size(); ifatjet++) {
    auto fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[ifatjet], FatJet_eta[ifatjet],
      FatJet_phi[ifatjet], FatJet_mass[ifatjet]);
    if (fatjet_tlv.Pt() < 250 || fabs(fatjet_tlv.Eta()) > max_bjet_eta) continue;
    if (FatJet_msoftdrop.at(ifatjet) < 30) continue;
    if (FatJet_jetId[ifatjet] != 6) continue;
    // do cleaning in 0.8 cone only for hadronic taus
    if ((pairType >=0 && pairType <= 2 && fatjet_tlv.DeltaR(dau2_tlv) < 0.8) ||
        (pairType == 2 && fatjet_tlv.DeltaR(dau1_tlv) < 0.8) )
      continue;
    fatjet_indexes.push_back(jet_idx_btag({(int) ifatjet, FatJet_particleNet_XbbVsQCD.at(ifatjet)}));
  }
  if (fatjet_indexes.size() != 0) {
    hasBoostedAK8_ = 1;
    std::stable_sort(fatjet_indexes.begin(), fatjet_indexes.end(), jetSort);
    fatjet_idx = fatjet_indexes[0].idx;

    TLorentzVector fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[fatjet_idx], FatJet_eta[fatjet_idx],
                            FatJet_phi[fatjet_idx], FatJet_mass[fatjet_idx]);

    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet_index = all_jet_indexes[ijet];
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet_index], Jet_eta[jet_index],
                          Jet_phi[jet_index], Jet_mass[jet_index]);
      
      if (jet_tlv.DeltaR(fatjet_tlv) < 0.8) {
        if (fatjet2jet1_idx == -1)
          fatjet2jet1_idx = jet_index;
        else if (fatjet2jet2_idx == -1)
          fatjet2jet2_idx = jet_index;
        else
          break; // jets are ordered in pt by construction
      }
    }
  }

  if (jet_indexes.size() >= 2) {
    hasResolvedAK4_ = 1;

    std::stable_sort(jet_indexes.begin(), jet_indexes.end(), jetSort);

    auto htt_tlv = dau1_tlv + dau2_tlv;
    std::vector <float> HHbtag_jet_pt_, HHbtag_jet_eta_, HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_;
    std::vector <float> HHbtag_jet_htt_deta_, HHbtag_jet_htt_dphi_, HHbtag_jet_btagOutput_;

    for (auto &jet : jet_indexes) {
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet.idx], Jet_eta[jet.idx], Jet_phi[jet.idx], Jet_mass[jet.idx]);
      HHbtag_jet_pt_.push_back(jet_tlv.Pt());
      HHbtag_jet_eta_.push_back(jet_tlv.Eta());
      HHbtag_rel_jet_M_pt_.push_back(jet_tlv.M() / jet_tlv.Pt());
      HHbtag_rel_jet_E_pt_.push_back(jet_tlv.E() / jet_tlv.Pt());
      HHbtag_jet_htt_deta_.push_back(htt_tlv.Eta() - jet_tlv.Eta());
      HHbtag_jet_htt_dphi_.push_back(ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, jet_tlv));
      HHbtag_jet_btagOutput_.push_back(jet.btag);
    }

    auto HHbtag_htt_met_dphi_ = (float) ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, met_tlv);
    auto HHbtag_htt_scalar_pt_ = (float) (dau1_tlv.Pt() + dau2_tlv.Pt());
    auto HHbtag_rel_met_pt_htt_pt_ = (float) met_tlv.Pt() / HHbtag_htt_scalar_pt_;
    auto HHbtag_htt_pt_ = (float) htt_tlv.Pt();
    auto HHbtag_htt_eta_ = (float) htt_tlv.Eta();
    int HHbtag_eraid_ = -1;

    if (year_ == 2022) {
      if (runPeriod_ == "preEE")
        HHbtag_eraid_ = 0;
      else if (runPeriod_ == "postEE")
        HHbtag_eraid_ = 1;
    }
    else if (year_ == 2023) {
      if (runPeriod_ == "preBPix")
        HHbtag_eraid_ = 2;
      else if (runPeriod_ == "postBPix")
        HHbtag_eraid_ = 3;
    }
    else
      throw std::invalid_argument("HHJetsInterface::HHBtag - Invalid year");

    auto HHbtag_scores = HHbtagger_.GetScore(HHbtag_jet_pt_, HHbtag_jet_eta_,
      HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_, HHbtag_jet_htt_deta_,
      HHbtag_jet_btagOutput_, HHbtag_jet_htt_dphi_, HHbtag_eraid_, pairType,
      HHbtag_htt_pt_, HHbtag_htt_eta_, HHbtag_htt_met_dphi_,
      HHbtag_rel_met_pt_htt_pt_, HHbtag_htt_scalar_pt_, event);

    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      all_HHbtag_scores[jet_indexes[ijet].idx] = HHbtag_scores[ijet];
    }
    std::vector <jet_idx_btag> jet_indexes_hhbtag;
    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      jet_indexes_hhbtag.push_back(jet_idx_btag({jet_indexes[ijet].idx, HHbtag_scores[ijet]}));
    }

    // Choose b-jets with HHBtag or with tagger
    if (!bypass_HHBTag_) {
      std::stable_sort(jet_indexes_hhbtag.begin(), jet_indexes_hhbtag.end(), jetSort);
      bjet1_idx = jet_indexes_hhbtag[0].idx;
      bjet2_idx = jet_indexes_hhbtag[1].idx;
    }
    else {
      bjet1_idx = jet_indexes[0].idx;
      bjet2_idx = jet_indexes[1].idx;
    }

    // Order 2 selected b-jets by pt
    if (Jet_pt[bjet1_idx] < Jet_pt[bjet2_idx]) {
        auto aux = bjet1_idx;
        bjet1_idx = bjet2_idx;
        bjet2_idx = aux;
    }
  }

  if (all_jet_indexes.size() >= 2) { // 0 fatjet2jet + 0 bjets + 2 vbf jets
    std::vector <jet_pair_mass> vbfjet_indexes;
    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet1_index = all_jet_indexes[ijet];
      auto jet1_tlv = TLorentzVector();
      jet1_tlv.SetPtEtaPhiM(Jet_pt[jet1_index], Jet_eta[jet1_index],
                            Jet_phi[jet1_index], Jet_mass[jet1_index]);
      
      if (hasBoostedAK8_ && (jet1_index == fatjet2jet1_idx || jet1_index == fatjet2jet2_idx))
        continue;
      if (hasResolvedAK4_ && (jet1_index == bjet1_idx || jet1_index == bjet2_idx))
        continue;
      
      for (size_t jjet = ijet + 1; jjet < all_jet_indexes.size(); jjet++) {
        auto jet2_index = all_jet_indexes[jjet];
        auto jet2_tlv = TLorentzVector();
        jet2_tlv.SetPtEtaPhiM(Jet_pt[jet2_index], Jet_eta[jet2_index],
                              Jet_phi[jet2_index], Jet_mass[jet2_index]);
        
        if (hasBoostedAK8_ && (jet2_index == fatjet2jet1_idx || jet2_index == fatjet2jet2_idx))
          continue;
        if (hasResolvedAK4_ && (jet2_index == bjet1_idx || jet2_index == bjet2_idx))
          continue;
        if (Jet_pt[jet1_index] < 20 || Jet_pt[jet2_index] < 20)
          continue; // cut was originally 30, keeping it at 20 for initial studies
        
        auto jj_tlv = jet1_tlv + jet2_tlv;
        vbfjet_indexes.push_back(jet_pair_mass({jet1_index, jet2_index, (float) jj_tlv.M()}));
      }
    }
    if (vbfjet_indexes.size() > 0) {
      std::stable_sort(vbfjet_indexes.begin(), vbfjet_indexes.end(), jetPairSort);
      vbfjet1_idx = vbfjet_indexes[0].idx1;
      vbfjet2_idx = vbfjet_indexes[0].idx2;
      hasVBFAK4_ = 1;

      if (Jet_pt[vbfjet1_idx] < Jet_pt[vbfjet2_idx]) {
        auto aux = vbfjet1_idx;
        vbfjet1_idx = vbfjet2_idx;
        vbfjet2_idx = aux;
      }
    }
  }

  // additional central and forward jets
  for (auto & ijet : all_jet_indexes) {
    if ((int) ijet == bjet1_idx || (int) ijet == bjet2_idx
        || (int) ijet == vbfjet1_idx || (int) ijet == vbfjet2_idx
        || (int) ijet == fatjet2jet1_idx || (int) ijet == fatjet2jet2_idx)
      continue;
    if (fabs(Jet_eta[ijet]) < max_bjet_eta)
      ctjet_indexes.push_back(ijet);
    else if (fabs(Jet_eta[ijet]) < max_vbfjet_eta && Jet_pt[ijet] > 30)
      fwjet_indexes.push_back(ijet);
  }

  return jets_output({all_HHbtag_scores, hasResolvedAK4_, bjet1_idx, bjet2_idx,
    hasVBFAK4_, vbfjet1_idx, vbfjet2_idx, ctjet_indexes, fwjet_indexes,
    hasBoostedAK8_, fatjet_idx, fatjet2jet1_idx, fatjet2jet2_idx});
}

// this definition uses the new fatjet without subjet apporoach
jets_output HHJetsInterface::GetHHJetsWrapper(
  unsigned long long int event, bool useCHSjets,
  fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
  iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
  fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
  fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
  int pairType, int dau1_index, int dau2_index,
  fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
  fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
  fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
  float MET_pt, float MET_phi)
{
  std::vector<int> dummyMuVecI(Muon_pt.size(), -999);
  std::vector<int> dummyEleVecI(Electron_pt.size(), -999);
  std::vector<int> dummyTauVecI(Tau_pt.size(), -999);
  std::vector<float> dummyTauVecF(Tau_pt.size(), -999.);
  leps lptns = GetLeps(Muon_pt, Muon_eta, Muon_phi, Muon_mass, dummyMuVecI,
                       Electron_pt, Electron_eta, Electron_phi, Electron_mass, dummyEleVecI,
                       Tau_pt, Tau_eta, Tau_phi, Tau_mass, dummyTauVecI, dummyTauVecI,
                       dummyTauVecF, dummyTauVecF, dummyTauVecF,
                       dau1_index, dau2_index, pairType);

  if (useCHSjets) 
    return GetHHJets(event, pairType,
      Jet_pt, Jet_eta, Jet_phi, Jet_mass,
      Jet_puId, Jet_jetId, Jet_btag,
      FatJet_pt, FatJet_eta, FatJet_phi, FatJet_mass,
      FatJet_msoftdrop, FatJet_particleNet_XbbVsQCD, FatJet_jetId,
      lptns.dau1_pt, lptns.dau1_eta, lptns.dau1_phi, lptns.dau1_mass,
      lptns.dau2_pt, lptns.dau2_eta, lptns.dau2_phi, lptns.dau2_mass,
      MET_pt, MET_phi);
  else 
    return GetHHJets(event, pairType,
      Jet_pt, Jet_eta, Jet_phi, Jet_mass,
      Jet_jetId, Jet_btag,
      FatJet_pt, FatJet_eta, FatJet_phi, FatJet_mass,
      FatJet_msoftdrop, FatJet_particleNet_XbbVsQCD, FatJet_jetId,
      lptns.dau1_pt, lptns.dau1_eta, lptns.dau1_phi, lptns.dau1_mass,
      lptns.dau2_pt, lptns.dau2_eta, lptns.dau2_phi, lptns.dau2_mass,
      MET_pt, MET_phi);
}
