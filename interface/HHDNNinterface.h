#ifndef HHDNNinterface_h
#define HHDNNinterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHDNNinterface                                                                                       //
//                                                                                                                //
//   Class to compute DNN output.                                                                                 //
//   Original DNN packages from:                                                                                  //
//    - https://github.com/GilesStrong/cms_hh_proc_interface                                                      //
//    - https://github.com/GilesStrong/cms_hh_tf_inference                                                        //
//    - https://github.com/GilesStrong/cms_runII_dnn_models                                                       //
//   Modified from https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/interface/DNNKLUBinterface.h
//                                                                                                                //
//   Author: Jaime León Holgado (CIEMAT)                                                                          //
//   Date  : August 2021                                                                                          //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <map>

// Math libraries
#include <Math/VectorUtil.h>
#include <Math/LorentzVector.h>
#include <Math/PxPyPzM4D.h>

// ROOT libraries
#include "TLorentzVector.h"

// DNN
#include "cms_hh_tf_inference/inference/interface/inf_wrapper.hh"
#include "cms_hh_proc_interface/processing/interface/feat_comp.hh"
#include "cms_hh_proc_interface/processing/interface/evt_proc.hh"

// Utils
#include "Tools/Tools/interface/HHUtils.h"
#include "Tools/Tools/interface/lester_mt2_bisect.h"

// Using names
using DNNVector = ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<float>>;

// DNN Constants
const double E_MASS  = 0.0005109989; //GeV
const double MU_MASS = 0.1056583715; //GeV

// HHDNNinterface class
class HHDNNinterface {

  public:
    HHDNNinterface (std::string model_dir, std::vector<std::string> requested,
      std::vector<float> target_kls, int year);
    ~HHDNNinterface ();
    
    void SetGlobalInputs(int year);

    void SetEventInputs(int channel, int is_boosted, int nvbf, unsigned long long int eventn,
      TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
      TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
      float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
      float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
      float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
      float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2);

    std::vector<float> GetPredictions();

    std::vector<float> GetDeafultInputs(
      int channel, int is_boosted, int nvbf, unsigned long long int eventn,
      TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
      TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
      float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
      float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
      float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
      float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2);

    std::vector<float> GetPredictionsWithInputs(
      int channel, int is_boosted, int nvbf, unsigned long long int eventn,
      TLorentzVector b1, TLorentzVector b2, TLorentzVector l1, TLorentzVector l2, 
      TLorentzVector vbf1, TLorentzVector vbf2, TLorentzVector met, TLorentzVector svfit, 
      float KinFitMass, float KinFitChi2, bool KinFitConv, bool SVfitConv, float MT2,
      float deepFlav1, float deepFlav2, float CvsL_b1, float CvsL_b2, float CvsL_vbf1, float CvsL_vbf2,
      float CvsB_b1, float CvsB_b2, float CvsB_vbf1, float CvsB_vbf2,
      float HHbtag_b1, float HHbtag_b2, float HHbtag_vbf1, float HHbtag_vbf2);

    // wrapper function to be used in the .py that works with non-flat pNTples
    // and reads the vector branches using the indexes of teh selected objects
    fRVec get_dnn_outputs(
      int pairType, int hasBoostedAK8, int event,
      int dau1_index, int dau2_index, int bjet1_index, int bjet2_index,
      bool hasResolvedAK4, int vbfjet1_index, int vbfjet2_index,
      fRVec muon_pt, fRVec muon_eta, fRVec muon_phi, fRVec muon_mass,
      fRVec electron_pt, fRVec electron_eta, fRVec electron_phi, fRVec electron_mass,
      fRVec tau_pt, fRVec tau_eta, fRVec tau_phi, fRVec tau_mass,
      fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass,
      float htt_sv_pt, float htt_sv_eta, float htt_sv_phi, float htt_sv_mass,
      float HHKinFit_mass, float HHKinFit_chi2, float met_pt, float met_phi,
      fRVec Jet_btagDeepFlavB, fRVec Jet_btagDeepFlavCvL, fRVec Jet_btagDeepFlavCvB,
      fRVec Jet_HHbtag);

    // wrapper function to be used in the .py that works with flat pNTples
    // and already gets the information of the selected objects
    fRVec get_dnn_outputs(
      int pairType, int event,
      bool hasResolvedAK4, bool hasBoostedAK8, bool hasVBFAK4,
      float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
      float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
      float bjet1_pt, float bjet1_eta, float bjet1_phi, float bjet1_mass,
      float bjet2_pt, float bjet2_eta, float bjet2_phi, float bjet2_mass,
      float bjet1_btagPNetB, float bjet1_btagPNetCvB, float bjet1_btagPNetCvL, float bjet1_hhbtag,
      float bjet2_btagPNetB, float bjet2_btagPNetCvB, float bjet2_btagPNetCvL, float bjet2_hhbtag,
      float vbfjet1_pt, float vbfjet1_eta, float vbfjet1_phi, float vbfjet1_mass,
      float vbfjet2_pt, float vbfjet2_eta, float vbfjet2_phi, float vbfjet2_mass,
      float vbfjet1_btagPNetCvB, float vbfjet1_btagPNetCvL, float vbfjet1_hhbtag,
      float vbfjet2_btagPNetCvB, float vbfjet2_btagPNetCvL, float vbfjet2_hhbtag,
      float htt_sv_pt, float htt_sv_eta, float htt_sv_phi, float htt_sv_mass,
      float HHKinFit_mass, float HHKinFit_chi2, float met_pt, float met_phi);

  private:
    InfWrapper wrapper_;
    EvtProc    evt_proc_;
    std::vector<float> target_kls_;

    // Declare input variables to DNN
    DNNVector DNN_b_1_, DNN_b_2_, DNN_l_1_, DNN_l_2_, DNN_met_, DNN_svfit_, DNN_vbf_1_, DNN_vbf_2_;
    float DNN_kinfit_mass_, DNN_kinfit_chi2_, DNN_mt2_;
    float DNN_b_1_deepflav_, DNN_b_2_deepflav_;
    float DNN_b_1_hhbtag_, DNN_b_1_cvsl_, DNN_b_1_cvsb_;
    float DNN_b_2_hhbtag_, DNN_b_2_cvsl_, DNN_b_2_cvsb_;
    float DNN_vbf_1_hhbtag_, DNN_vbf_1_cvsl_, DNN_vbf_1_cvsb_;
    float DNN_vbf_2_hhbtag_, DNN_vbf_2_cvsl_, DNN_vbf_2_cvsb_;
    int DNN_is_boosted_, DNN_n_vbf_;
    unsigned long long int DNN_evt_;
    bool DNN_svfit_conv_, DNN_hh_kinfit_conv_;
    Channel DNN_e_channel_;
    Year DNN_e_year_;
    Spin DNN_spin_;
    float DNN_klambda_;
    float DNN_res_mass_;
};

#endif // HHDNNinterface_h
