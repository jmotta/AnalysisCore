#ifndef HHJetsInterface_h
#define HHJetsInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHJetsInterface                                                                                        //
//                                                                                                                //
//   Class to compute HHbtag output.                                                                              //
//                                                                                                                //
//   Author: Jaime León Holgado                                                                                   //
//   Date  : June 2021                                                                                            //
//                                                                                                                //
//   modified from                                                                                                //
//                                                                                                                //
//   https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/interface/HHbtagKLUBinterface.h                       //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

// HHbtag libraries
#include "HHTools/HHbtag/interface/HH_BTag.h"

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

// Utils
#include "Tools/Tools/interface/HHUtils.h"

// HHJetsInterface class
class HHJetsInterface {

  public:
    HHJetsInterface (std::string model_0, std::string model_1, int year, std::string runPeriod, bool extEtaAcc,
                     bool bypass_HHBTag, float btag_algo_cut);
    ~HHJetsInterface ();

  // definition compatible with Run-2 analyses using CHS jets and need Jet_puId
  // this definition uses the new fatjet without subjet apporoach
  jets_output GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float MET_pt, float MET_phi);

  // defintiion compatible with Run-3 analyses using PUPPI jets and don't need Jet_puId
  // this definition uses the new fatjet without subjet apporoach
  jets_output GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float MET_pt, float MET_phi);
  
  // this definition uses the new fatjet without subjet apporoach
  jets_output GetHHJetsWrapper(
    unsigned long long int event, bool useCHSjets,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD, fRVec FatJet_jetId,
    int pairType, int dau1_index, int dau2_index,
    fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
    fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
    fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
    float MET_pt, float MET_phi);

  private:
    hh_btag::HH_BTag HHbtagger_;
    int year_;
    std::string runPeriod_;
    float max_bjet_eta = 2.4;
    float max_vbfjet_eta = 4.7;
    bool bypass_HHBTag_;
    float btag_algo_cut_;

    bool applyTrg_;
    bool applyOfln_;
};

#endif // HHJetsInterface
