#ifndef HHLeptonInterface_h
#define HHLeptonInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHLeptonInterface                                                                                      //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

// Utils
#include "Tools/Tools/interface/HHUtils.h"

// HHLeptonInterface class
class HHLeptonInterface {

  public:
    HHLeptonInterface (int vvvl_vsjet, int vl_vse, int vvl_vse, int t_vsmu, int vl_vsmu, bool applyOflnIso);

    ~HHLeptonInterface ();

    lepton_output get_dau_indexes(
      fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
      fRVec Muon_pfRelIso04_all, fRVec Muon_dxy, fRVec Muon_dz,
      bRVec Muon_mediumId, bRVec Muon_tightId, iRVec Muon_charge,
      fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
      bRVec Electron_mvaIso_WP80, bRVec Electron_mvaNoIso_WP90,
      bRVec Electron_mvaIso_WP90, fRVec Electron_pfRelIso03_all,
      fRVec Electron_dxy, fRVec Electron_dz, iRVec Electron_charge, fRVec Electron_mvaIso,
      iRVec Electron_seediEtaOriX, iRVec Electron_seediPhiOriY, bool EEleakVeto,
      fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
      iRVec Tau_tauIdVSmu, iRVec Tau_tauIdVSe,
      iRVec Tau_tauIdVSjet, fRVec Tau_rawTauIdVSjet,
      fRVec Tau_dz, iRVec Tau_decayMode, iRVec Tau_charge
    );

  private:
    int vvvl_vsjet_;
    int vl_vse_;
    int vvl_vse_;
    int t_vsmu_;
    int vl_vsmu_;

    bool applyTrg_;
    bool applyOfln_;
    bool applyOflnIso_;
};

#endif // HHLeptonInterface_h
