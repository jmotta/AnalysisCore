#ifndef HHUtils_h
#define HHUtils_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHUtils                                                                                                //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

//------------------------------------------------------------------------
// general types used everywhere
typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;
typedef ROOT::VecOps::RVec<uint64_t> ui64RVec;

//------------------------------------------------------------------------
// stuctures and functions needed for trigger checking and matching
struct trig_req {
    bool pass;
    std::string dataset;
    std::vector<float> pt;
    std::vector<float> eta;
    std::vector<int> id;
    std::vector<std::vector<int>> bits;
    std::vector<std::vector<int>> addbits;
};

struct trigger_hlt_matched_output {
    bool trigger_passed;
    int HLTMatchedJets;
};

struct trigger_output {
    bool pass_triggers;
    bool isTauTauJetTrigger;
    bool isQuadJetTrigger;
    bool isVBFtrigger;
    int QuadJetMatchedToHLT;
};

bool match_trigger_object(float off_eta, float off_phi, int obj_id,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<int> bits, std::vector<int> addbits, int off_id);

bool pass_trigger(
    std::vector<TLorentzVector> off_objs,
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi);

bool pass_trigger(
    std::vector<TLorentzVector> off_objs, std::vector<int> off_ids,
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi);

// trigger_hlt_matched_output pass_trigger(
//     std::vector<TLorentzVector> mandatory_objs, std::vector<TLorentzVector> optional_objs, 
//     std::vector<trig_req> triggers, iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi);

struct triggerSF_output {
    double trigSF            = 1.0;
    double trigSF_muUp       = 1.0;
    double trigSF_muDown     = 1.0;
    double trigSF_eleUp      = 1.0;
    double trigSF_eleDown    = 1.0;
    double trigSF_DM0Up      = 1.0;
    double trigSF_DM1Up      = 1.0;
    double trigSF_DM10Up     = 1.0;
    double trigSF_DM11Up     = 1.0;
    double trigSF_DM0Down    = 1.0;
    double trigSF_DM1Down    = 1.0;
    double trigSF_DM10Down   = 1.0;
    double trigSF_DM11Down   = 1.0;
    double trigSF_vbfjetUp   = 1.0;
    double trigSF_vbfjetDown = 1.0;
    double trigSF_jetUp      = 1.0;
    double trigSF_jetDown    = 1.0;
    double trigSF_nojetSF    = 1.0;

    void update(double evt_trigSF, double evt_trigSF_muUp, 
                double evt_trigSF_muDown, double evt_trigSF_eleUp,
                double evt_trigSF_eleDown, double evt_trigSF_DM0Up,
                double evt_trigSF_DM1Up, double evt_trigSF_DM10Up,
                double evt_trigSF_DM11Up, double evt_trigSF_DM0Down,
                double evt_trigSF_DM1Down, double evt_trigSF_DM10Down,
                double evt_trigSF_DM11Down, double evt_trigSF_vbfjetUp,
                double evt_trigSF_vbfjetDown, double evt_trigSF_jetUp,
                double evt_trigSF_jetDown, double evt_trigSF_nojetSF)
    {
        trigSF            = evt_trigSF;
        trigSF_muUp       = evt_trigSF_muUp;
        trigSF_muDown     = evt_trigSF_muDown;
        trigSF_eleUp      = evt_trigSF_eleUp;
        trigSF_eleDown    = evt_trigSF_eleDown;
        trigSF_DM0Up      = evt_trigSF_DM0Up;
        trigSF_DM1Up      = evt_trigSF_DM1Up;
        trigSF_DM10Up     = evt_trigSF_DM10Up;
        trigSF_DM11Up     = evt_trigSF_DM11Up;
        trigSF_DM0Down    = evt_trigSF_DM0Down;
        trigSF_DM1Down    = evt_trigSF_DM1Down;
        trigSF_DM10Down   = evt_trigSF_DM10Down;
        trigSF_DM11Down   = evt_trigSF_DM11Down;
        trigSF_vbfjetUp   = evt_trigSF_vbfjetUp;
        trigSF_vbfjetDown = evt_trigSF_vbfjetDown;
        trigSF_jetUp      = evt_trigSF_jetUp;
        trigSF_jetDown    = evt_trigSF_jetDown;
        trigSF_nojetSF    = evt_trigSF_nojetSF;
    }
};

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHLeptonInterface
struct tau_pair {
    int index1;
    float iso1;
    float pt1;
    int index2;
    float iso2;
    float pt2;
};

struct lepton_output {
    int pairType;
    int dau1_index;
    int dau2_index;
    int isOS;

    // Method to udpate values of lepton_output
    void update(int evt_pairType, int evt_dau1_index,
                int evt_dau2_index, int evt_isOS)
    {
        pairType = evt_pairType;
        dau1_index = evt_dau1_index;
        dau2_index = evt_dau2_index;
        isOS = evt_isOS;
    }
};

// pairSorting for MVA isolations: higher score --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortMVA (const tau_pair& pA, const tau_pair& pB);

// pairSorting for Raw isolations: lower iso value --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortRawIso (const tau_pair& pA, const tau_pair& pB);

// pairSorting for Lep+Tauh:
//  - leg1 (lepton): lower iso value --> more isolated
//  - leg2 (tauh)  : higher score    --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortHybrid (const tau_pair& pA, const tau_pair& pB);

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHJetsInterface
struct jet_idx_btag {
    int idx;
    float btag;
};

struct jet_pair_mass {
    int idx1;
    int idx2;
    float inv_mass;
};

struct jets_output {
    std::vector <float> hhbtag;
    int hasResolvedAK4;
    int bjet_idx1;
    int bjet_idx2;
    int hasVBFAK4;
    int vbfjet_idx1;
    int vbfjet_idx2;
    std::vector<int> ctjet_indexes;
    std::vector<int> fwjet_indexes;
    int hasBoostedAK8;
    int fatjet_idx;
    int fatjet2jet1_idx;
    int fatjet2jet2_idx;
};

struct leps {
    float dau1_pt = -999.;
    float dau1_eta = -999.;
    float dau1_phi = -999.;
    float dau1_mass = -999.;
    float dau1_tauIdVSe = -999.;
    float dau1_tauIdVSmu = -999.;
    float dau1_tauIdVSjet = -999.;
    int   dau1_charge = -999;
    int   dau1_DM = -999;
    float dau2_pt = -999.;
    float dau2_eta = -999.;
    float dau2_phi = -999.;
    float dau2_mass = -999.;
    float dau2_tauIdVSe = -999.;
    float dau2_tauIdVSmu = -999.;
    float dau2_tauIdVSjet = -999.;
    int   dau2_charge = -999;
    int   dau2_DM = -999;
};

struct leps_miscellanea {
    float dau1_dxy = -999.;
    float dau1_dxyErr = -999.;
    float dau1_dz = -999.;
    float dau1_dzErr = -999.;
    float dau1_mvaIso = -999.;
    float dau1_mvaIso_WP80 = -999.;
    float dau1_mvaIso_WP90 = -999.;
    float dau1_mvaNoIso = -999.;
    float dau1_mvaNoIso_WP80 = -999.;
    float dau1_mvaNoIso_WP90 = -999.;
    float dau1_mediumId = -999.;
    float dau1_tightId = -999.;
    float dau1_pfRelIso04_all = -999.;
    float dau1_eleIdx = -999.;
    float dau1_muIdx = -999.;
    float dau1_genPartFlav = -999.;
    float dau1_genPartIdx = -999.;
    float dau1_idAntiMu = -999.;
    float dau1_idDecayModeNewDMs = -999.;
    float dau1_nSVs = -999.;
    float dau1_rawDeepTau2018v2p5VSe = -999.;
    float dau1_rawDeepTau2018v2p5VSjet = -999.;
    float dau1_rawDeepTau2018v2p5VSmu = -999.;
    float dau2_dxy = -999.;
    float dau2_dxyErr = -999.;
    float dau2_dz = -999.;
    float dau2_dzErr = -999.;
    float dau2_mvaIso = -999.;
    float dau2_mvaIso_WP80 = -999.;
    float dau2_mvaIso_WP90 = -999.;
    float dau2_mvaNoIso = -999.;
    float dau2_mvaNoIso_WP80 = -999.;
    float dau2_mvaNoIso_WP90 = -999.;
    float dau2_mediumId = -999.;
    float dau2_tightId = -999.;
    float dau2_pfRelIso04_all = -999.;
    float dau2_eleIdx = -999.;
    float dau2_muIdx = -999.;
    float dau2_genPartFlav = -999.;
    float dau2_genPartIdx = -999.;
    float dau2_idAntiMu = -999.;
    float dau2_idDecayModeNewDMs = -999.;
    float dau2_nSVs = -999.;
    float dau2_rawDeepTau2018v2p5VSe = -999.;
    float dau2_rawDeepTau2018v2p5VSjet = -999.;
    float dau2_rawDeepTau2018v2p5VSmu = -999.;
};

struct jets {
    float    bjet1_pt = -999.;
    float    bjet1_eta = -999.;
    float    bjet1_phi = -999.;
    float    bjet1_mass = -999.;
    float    bjet1_btag = -999.;
    float    bjet1_hhbtag = -999.;
    float    bjet1_btagCvB = -999.;
    float    bjet1_btagCvL = -999.;
    float    bjet1_resolution = -999.;
    uint64_t bjet1_smearseed = 0;
    float    bjet2_pt = -999.;
    float    bjet2_eta = -999.;
    float    bjet2_phi = -999.;
    float    bjet2_mass = -999.;
    float    bjet2_btag = -999.;
    float    bjet2_hhbtag = -999.;
    float    bjet2_btagCvB = -999.;
    float    bjet2_btagCvL = -999.;
    float    bjet2_resolution = -999.;
    uint64_t bjet2_smearseed = 0;
    float    fatbjet_pt = -999.;
    float    fatbjet_eta = -999.;
    float    fatbjet_phi = -999.;
    float    fatbjet_mass = -999.;
    float    fatbjet_btag = -999.;
    float    vbfjet1_pt = -999.;
    float    vbfjet1_eta = -999.;
    float    vbfjet1_phi = -999.;
    float    vbfjet1_mass = -999.;
    float    vbfjet1_hhbtag = -999.;
    float    vbfjet1_btagCvB = -999.;
    float    vbfjet1_btagCvL = -999.;
    float    vbfjet2_pt = -999.;
    float    vbfjet2_eta = -999.;
    float    vbfjet2_phi = -999.;
    float    vbfjet2_mass = -999.;
    float    vbfjet2_hhbtag = -999.;
    float    vbfjet2_btagCvB = -999.;
    float    vbfjet2_btagCvL = -999.;
};

struct jets_miscellanea {
    float bjet1_PNetRegPtRawCorr = -999.;
    float bjet1_PNetRegPtRawCorrNeutrino = -999.;
    float bjet1_PNetRegPtRawRes = -999.;
    float bjet1_btagPNetB = -999.;
    float bjet1_btagPNetCvB = -999.;
    float bjet1_btagPNetCvL = -999.;
    float bjet1_btagPNetQvG = -999.;
    float bjet1_btagPNetTauVJet = -999.;
    float bjet1_btagRobustParTAK4B = -999.;
    float bjet1_btagRobustParTAK4CvB = -999.;
    float bjet1_btagRobustParTAK4CvL = -999.;
    float bjet1_btagRobustParTAK4QG = -999.;
    float bjet1_genJetIdx = -999.;
    float bjet1_rawFactor = -999.;
    float bjet2_PNetRegPtRawCorr = -999.;
    float bjet2_PNetRegPtRawCorrNeutrino = -999.;
    float bjet2_PNetRegPtRawRes = -999.;
    float bjet2_btagPNetB = -999.;
    float bjet2_btagPNetCvB = -999.;
    float bjet2_btagPNetCvL = -999.;
    float bjet2_btagPNetQvG = -999.;
    float bjet2_btagPNetTauVJet = -999.;
    float bjet2_btagRobustParTAK4B = -999.;
    float bjet2_btagRobustParTAK4CvB = -999.;
    float bjet2_btagRobustParTAK4CvL = -999.;
    float bjet2_btagRobustParTAK4QG = -999.;
    float bjet2_genJetIdx = -999.;
    float bjet2_rawFactor = -999.;
    float fatjet_genJetAK8Idx = -999.;
    float fatjet_msoftdrop = -999.;
    float fatjet_particleNetWithMass_HbbvsQCD = -999.;
    float fatjet_particleNet_XbbVsQCD = -999.;
    float fatjet_particleNet_XteVsQCD = -999.;
    float fatjet_particleNet_XtmVsQCD = -999.;
    float fatjet_particleNet_XttVsQCD = -999.;
    float fatjet_particleNet_massCorr = -999.;
    float fatjet_rawFactor = -999.;
    float vbfjet1_PNetRegPtRawCorr = -999.;
    float vbfjet1_PNetRegPtRawCorrNeutrino = -999.;
    float vbfjet1_PNetRegPtRawRes = -999.;
    float vbfjet1_btagPNetB = -999.;
    float vbfjet1_btagPNetCvB = -999.;
    float vbfjet1_btagPNetCvL = -999.;
    float vbfjet1_btagPNetQvG = -999.;
    float vbfjet1_btagPNetTauVJet = -999.;
    float vbfjet1_btagRobustParTAK4B = -999.;
    float vbfjet1_btagRobustParTAK4CvB = -999.;
    float vbfjet1_btagRobustParTAK4CvL = -999.;
    float vbfjet1_btagRobustParTAK4QG = -999.;
    float vbfjet1_genJetIdx = -999.;
    float vbfjet1_rawFactor = -999.;
    float vbfjet2_PNetRegPtRawCorr = -999.;
    float vbfjet2_PNetRegPtRawCorrNeutrino = -999.;
    float vbfjet2_PNetRegPtRawRes = -999.;
    float vbfjet2_btagPNetB = -999.;
    float vbfjet2_btagPNetCvB = -999.;
    float vbfjet2_btagPNetCvL = -999.;
    float vbfjet2_btagPNetQvG = -999.;
    float vbfjet2_btagPNetTauVJet = -999.;
    float vbfjet2_btagRobustParTAK4B = -999.;
    float vbfjet2_btagRobustParTAK4CvB = -999.;
    float vbfjet2_btagRobustParTAK4CvL = -999.;
    float vbfjet2_btagRobustParTAK4QG = -999.;
    float vbfjet2_genJetIdx = -999.;
    float vbfjet2_rawFactor = -999.;
};

bool jetSort (const jet_idx_btag& jA, const jet_idx_btag& jB);

bool jetPairSort (const jet_pair_mass& jA, const jet_pair_mass& jB);

bool jetPtSort (const TLorentzVector& jA, const TLorentzVector& jB);

leps GetLeps(
    fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass, iRVec muoncharge,
    fRVec electronpt, fRVec electroneta, fRVec electronphi, fRVec electronmass, iRVec electroncharge,
    fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass, iRVec taucharge, iRVec taudecaymode,
    fRVec tadeeptauvse, fRVec taudeeptauvsmu, fRVec taudeeptauvsjet,
    int dau1_index, int dau2_index, int pairType);

leps_miscellanea GetLepsMisc(
    fRVec Muon_dxy, fRVec Muon_dxyErr, fRVec Muon_dz, fRVec Muon_dzErr, fRVec Muon_mediumId, fRVec Muon_pfRelIso04_all, fRVec Muon_tightId,
    fRVec Electron_dxy, fRVec Electron_dxyErr, fRVec Electron_dz, fRVec Electron_dzErr, fRVec Electron_mvaIso, fRVec Electron_mvaIso_WP80, fRVec Electron_mvaIso_WP90,
    fRVec Electron_mvaNoIso, fRVec Electron_mvaNoIso_WP80, fRVec Electron_mvaNoIso_WP90,
    fRVec Tau_dxy, fRVec Tau_dz, fRVec Tau_eleIdx, fRVec Tau_genPartFlav, fRVec Tau_genPartIdx, fRVec Tau_idAntiMu, fRVec Tau_idDecayModeNewDMs,
    fRVec Tau_muIdx, fRVec Tau_nSVs, fRVec Tau_rawDeepTau2018v2p5VSe, fRVec Tau_rawDeepTau2018v2p5VSjet, fRVec Tau_rawDeepTau2018v2p5VSmu,
    int dau1_index, int dau2_index, int pairType);

std::pair<TLorentzVector, TLorentzVector> GetLepsTLV(
    int index1, int index2,
    fRVec lep1_pt, fRVec lep1_eta, fRVec lep1_phi, fRVec lep1_mass,
    fRVec lep2_pt, fRVec lep2_eta, fRVec lep2_phi, fRVec lep2_mass);

jets GetJets (
    bool hasresolvedak4, int bjet1_jetidx, int bjet2_jetidx,
    bool hasvbfak4, int vbfjet1_jetidx, int vbfjet2_jetidx,
    bool hasboostedak8, int fatjet_jetidx,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass, fRVec jet_btag, fRVec jet_hhbtag,
    fRVec jet_btagCvB, fRVec jet_btagCvL, fRVec jet_resolution, ui64RVec jet_smearseed,
    fRVec fatjet_pt, fRVec fatjet_eta, fRVec fatjet_phi, fRVec fatjet_mass, fRVec fatjet_btag);

jets_miscellanea GetJetsMisc(
    bool hasresolvedak4, int bjet1_jetidx, int bjet2_jetidx,
    bool hasvbfak4, int vbfjet1_jetidx, int vbfjet2_jetidx,
    bool hasboostedak8, int fatjet_jetidx,
    fRVec Jet_PNetRegPtRawCorr, fRVec Jet_PNetRegPtRawCorrNeutrino, fRVec Jet_PNetRegPtRawRes,
    fRVec Jet_btagPNetB, fRVec Jet_btagPNetCvB, fRVec Jet_btagPNetCvL, fRVec Jet_btagPNetQvG,
    fRVec Jet_btagPNetTauVJet, fRVec Jet_btagRobustParTAK4B, fRVec Jet_btagRobustParTAK4CvB,
    fRVec Jet_btagRobustParTAK4CvL, fRVec Jet_btagRobustParTAK4QG, fRVec Jet_genJetIdx,
    fRVec Jet_rawFactor, fRVec FatJet_genJetAK8Idx, fRVec FatJet_msoftdrop,
    fRVec FatJet_particleNetWithMass_HbbvsQCD, fRVec FatJet_particleNet_XbbVsQCD,
    fRVec FatJet_particleNet_XteVsQCD, fRVec FatJet_particleNet_XtmVsQCD,
    fRVec FatJet_particleNet_XttVsQCD, fRVec FatJet_particleNet_massCorr, fRVec FatJet_rawFactor);

std::pair<TLorentzVector, TLorentzVector> GetJetsTLV(
    int index1, int index2,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass);

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHVars
struct HHvariables {
    float Htt_pt = -999.;
    float Htt_eta = -999.;
    float Htt_phi = -999.;
    float Htt_mass = -999.;
    float Htt_met_pt = -999.;
    float Htt_met_eta = -999.;
    float Htt_met_phi = -999.;
    float Htt_met_mass = -999.;
    float Hbb_pt = -999.;
    float Hbb_eta = -999.;
    float Hbb_phi = -999.;
    float Hbb_mass = -999.;
    float HH_pt = -999.;
    float HH_eta = -999.;
    float HH_phi = -999.;
    float HH_mass = -999.;
    float HH_svfit_pt = -999.;
    float HH_svfit_eta = -999.;
    float HH_svfit_phi = -999.;
    float HH_svfit_mass = -999.;
    float HH_kinfit_mass = -999.;
    float HH_kinfit_chi2 = -999.;
    float VBFjj_deltaEta = -999.;
    float VBFjj_deltaPhi = -999.;
    float VBFjj_mass = -999.;
};

#endif // HHUtils_h
