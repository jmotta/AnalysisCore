#ifndef HHGenInterface_h
#define HHGenInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class DYstitching                                                                                            //
//                                                                                                                //
//                                                                                                                //
//   Author: Jona Motta                                                                                           //
//   UZH, March 2022                                                                                              //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

#include <vector>
#include <ROOT/RVec.hxx>
#include <TLorentzVector.h>

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;

struct gen_output {
    int PairType;
    
    // always the tautau decaying h
    int   H1_idx  = -99; 
    float H1_pt   = -99;
    float H1_eta  = -99;
    float H1_phi  = -99;
    float H1_mass = -99;

    // always the bb decaying h
    int   H2_idx  = -99;
    float H2_pt   = -99;
    float H2_eta  = -99;
    float H2_phi  = -99;
    float H2_mass = -99;

    // gen lep1
    int   L1_idx   = -99;
    float L1_pt    = -99;
    float L1_eta   = -99;
    float L1_phi   = -99;
    float L1_mass  = -99;
    int   L1_pdgid = -99;

    // gen lep2
    int   L2_idx   = -99;
    float L2_pt    = -99;
    float L2_eta   = -99;
    float L2_phi   = -99;
    float L2_mass  = -99;
    int   L2_pdgid = -99;

    // gen bjet1
    int   B1_idx   = -99;
    float B1_pt    = -99;
    float B1_eta   = -99;
    float B1_phi   = -99;
    float B1_mass  = -99;
    int   B1_pdgid = -99;

    // gen bjet2
    int   B2_idx   = -99;
    float B2_pt    = -99;
    float B2_eta   = -99;
    float B2_phi   = -99;
    float B2_mass  = -99;
    int   B2_pdgid = -99;

    // gen pairs HH, leplep, bb
    float HH_pt    = -99;
    float HH_eta   = -99;
    float HH_phi   = -99;
    float HH_mass  = -99;
    float HH_cts   = -99; // cos(theta*)

    float LL_pt    = -99;
    float LL_eta   = -99;
    float LL_phi   = -99;
    float LL_mass  = -99;

    float BB_pt    = -99;
    float BB_eta   = -99;
    float BB_phi   = -99;
    float BB_mass  = -99;
};

struct reco_objects {
    int   C1_idx  = -99;
    float C1_pt   = -99;
    float C1_eta  = -99;
    float C1_phi  = -99;
    float C1_mass = -99;

    int   C2_idx  = -99;
    float C2_pt   = -99;
    float C2_eta  = -99;
    float C2_phi  = -99;
    float C2_mass = -99;
};

struct gen_reco_output {
    int PairType;
    
    // always the tautau decaying h
    int   H1_idx  = -99; 
    float H1_pt   = -99;
    float H1_eta  = -99;
    float H1_phi  = -99;
    float H1_mass = -99;

    // always the bb decaying h
    int   H2_idx  = -99;
    float H2_pt   = -99;
    float H2_eta  = -99;
    float H2_phi  = -99;
    float H2_mass = -99;

    // gen lep1
    int   L1_idx   = -99;
    float L1_pt    = -99;
    float L1_eta   = -99;
    float L1_phi   = -99;
    float L1_mass  = -99;
    int   L1_pdgid = -99;

    // gen lep2
    int   L2_idx   = -99;
    float L2_pt    = -99;
    float L2_eta   = -99;
    float L2_phi   = -99;
    float L2_mass  = -99;
    int   L2_pdgid = -99;

    // gen bjet1
    int   B1_idx   = -99;
    float B1_pt    = -99;
    float B1_eta   = -99;
    float B1_phi   = -99;
    float B1_mass  = -99;
    int   B1_pdgid = -99;

    // gen bjet2
    int   B2_idx   = -99;
    float B2_pt    = -99;
    float B2_eta   = -99;
    float B2_phi   = -99;
    float B2_mass  = -99;
    int   B2_pdgid = -99;

    // gen pairs HH, leplep, bb
    float HH_pt    = -99;
    float HH_eta   = -99;
    float HH_phi   = -99;
    float HH_mass  = -99;
    float HH_cts   = -99; // cos(theta*)

    float LL_pt    = -99;
    float LL_eta   = -99;
    float LL_phi   = -99;
    float LL_mass  = -99;

    float BB_pt    = -99;
    float BB_eta   = -99;
    float BB_phi   = -99;
    float BB_mass  = -99;

    // gen-matched reco lep1
    int   T1_idx  = -99;
    float T1_pt   = -99;
    float T1_eta  = -99;
    float T1_phi  = -99;
    float T1_mass = -99;

    // gen-matched reco lep2
    int   T2_idx  = -99;
    float T2_pt   = -99;
    float T2_eta  = -99;
    float T2_phi  = -99;
    float T2_mass = -99;

    // gen-matched reco bjet1
    int   J1_idx  = -99;
    float J1_pt   = -99;
    float J1_eta  = -99;
    float J1_phi  = -99;
    float J1_mass = -99;

    // gen-matched reco bjet2
    int   J2_idx  = -99;
    float J2_pt   = -99;
    float J2_eta  = -99;
    float J2_phi  = -99;
    float J2_mass = -99;

    // gen.matched reco pairs leplep, bb
    float TT_pt   = -99;
    float TT_eta  = -99;
    float TT_phi  = -99;
    float TT_mass = -99;

    float JJ_pt   = -99;
    float JJ_eta  = -99;
    float JJ_phi  = -99;
    float JJ_mass = -99;
};

class HHGenInterface {

  public:
    HHGenInterface();
    ~HHGenInterface();

    gen_output get_gen_info(
        iRVec GenPart_statusFlags, iRVec GenPart_pdgId, 
        iRVec GenPart_status, iRVec GenPart_genPartIdxMother,
        fRVec GenPart_pt, fRVec GenPart_eta, fRVec GenPart_phi, fRVec GenPart_mass);

    gen_reco_output get_gen_reco(
        iRVec GenPart_statusFlags, iRVec GenPart_pdgId, 
        iRVec GenPart_status, iRVec GenPart_genPartIdxMother,
        fRVec GenPart_pt, fRVec GenPart_eta, fRVec GenPart_phi, fRVec GenPart_mass,
        iRVec GenVisTau_genPartIdxMother, iRVec GenVisTau_status,
        fRVec GenVisTau_pt, fRVec GenVisTau_eta, fRVec GenVisTau_phi, fRVec GenVisTau_mass,
        iRVec Muon_genPartIdx, fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
        iRVec Electron_genPartIdx, fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
        iRVec Tau_genPartIdx, fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
        iRVec GenJet_partonFlavour, fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass);

  private:
    gen_reco_output initFromGenInfo(gen_output genInput);
    
    reco_objects lep_gen2reco(
        iRVec GenPart_pdgId, iRVec GenPart_statusFlags, iRVec GenPart_status, iRVec GenPart_genPartIdxMother,
        gen_output genInput, iRVec GenVisTau_genPartIdxMother,
        fRVec GenVisTau_pt, fRVec GenVisTau_eta, fRVec GenVisTau_phi, fRVec GenVisTau_mass,
        iRVec l1genPartIdx, fRVec l1pt, fRVec l1eta, fRVec l1phi, fRVec l1mass,
        iRVec l2genPartIdx, fRVec l2pt, fRVec l2eta, fRVec l2phi, fRVec l2mass);

};

#endif // HHGenInterface_h


